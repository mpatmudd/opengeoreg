function I = alg_subsetByTransMatrix(I,T,varargin)
%%
% Description
% Subset image I using the translation matrix T. T gives the start indices
% and uvsize or usize/vsize give the dimensions of the subset.
%
% Sample call(s):
%
% I = alg_subsetByTransMatrix(I,T,uvsize)
% I = alg_subsetByTransMatrix(I,T,usize,vsize)
%
% Input(s)
%   Name          Description
%   I             The image to be subset
%   T             The translation matrix defining the start indices [no
%                 check is done to guarantee that the values are integers]
%   usize         Dimension of subset (first index)
%   vsize         Dimension of subset (second index)
%   uvsize        Dimension of subset (both indices)
% Output(s)
%   I             The subsetted image
usize = varargin{1};
if nargin > 3
    vsize = varargin{2};
else
    vsize = usize;
end

umin = 1 - T(1,3);
vmin = 1 - T(2,3);
umax = umin + usize - 1;
vmax = vmin + vsize - 1;

I = I(umin:umax,vmin:vmax);