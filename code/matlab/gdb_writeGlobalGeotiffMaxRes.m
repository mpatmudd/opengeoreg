function gdb_writeGlobalGeotiffMaxRes(gdbDir,key)
% Use the global fit to write a geotiff to file for the input key.
% This is done at the maximum mean  resolution of the original image or the
% reference image.

Hrx = gdb_getOrigImgTransform(gdbDir,key);
imgFile = gdb_getOriginalImageFile(gdbDir,key);

refFile = gdb_getRefFile(gdbDir);

outImgFile = fullfile(gdbDir,'img','geotiffs','global','maxres',[key,'.tif']);
% outMaskFile = fullfile(gdbDir,'img','geotiffs','global','maxres',['mask_',key,'.tif']);
img = imread(imgFile);
util_write_geotiff_using_homography_max_res(outImgFile,img,Hrx,refFile);
