function gdb_calcOverlapGraph(gdbDir)
% Utilize known point correspondences (fast scale) to build the overlap
% graph. Store the resulting matrix in */data/G.mat so that it does not
% have to be repeatedly calculated.

ppParam = gdb_loadPreProcParam(gdbDir);
scaleStrFast = ['scale',num2str(ppParam.fastScale)];
surfDir = fullfile(gdbDir,'data','surf',scaleStrFast);

keysAll = gdb_getAllKeys(gdbDir);
keysOutsideRef = gdb_getKeysOutsideRef(gdbDir);
keysAll = setdiff(keysAll,keysOutsideRef);
surfParamFast = gdb_loadSurfParamFast(gdbDir);
G = alg_buildOverlapGraph(surfDir,surfParamFast,keysAll);

outFile = fullfile(gdbDir,'data','G.mat');
save(outFile,'G');