function util_write_geotiff_using_homography_max_res(outFile,imgi,Hri,refFile)
% Write geotiff at maximum resolution

% Set up defaults
uMini = 1;
uMaxi = size(imgi,1);
vMini = 1;
vMaxi = size(imgi,2);

Ci = [uMini,uMini,uMaxi,uMaxi;vMini,vMaxi,vMini,vMaxi;1,1,1,1];
Cr = alg_transform_points_homography(Hri,Ci);

resInR = min([alg_res_i_in_j(Hri,Ci(1,1),Ci(2,1)),...
    alg_res_i_in_j(Hri,Ci(1,2),Ci(2,2)),...
    alg_res_i_in_j(Hri,Ci(1,3),Ci(2,3)),...
    alg_res_i_in_j(Hri,Ci(1,4),Ci(2,4))]);

uMinr = min(Cr(1,:));
uMaxr = max(Cr(1,:));
vMinr = min(Cr(2,:));
vMaxr = max(Cr(2,:));

uVectr = uMinr:resInR:uMaxr;
Ur = length(uVectr);
vVectr = vMinr:resInR:vMaxr;
Vr = length(vVectr);

if ndims(imgi) > 2
    subImage = repmat(uint8(0),[Ur,Vr,size(imgi,3)]);
    % Loop over bands
    for bb = 1:size(imgi,3)
        subImage(:,:,bb) = alg_warp_image_homography(Hri,uVectr,vVectr,imgi(:,:,bb));
    end
else
    subImage = alg_warp_image_homography(Hri,uVectr,vVectr,imgi);
end



% Determine the geo-spatial information
% xi = col + [-.5 .5];
% yi = row + [-.5 .5];
xi = [vVectr(1)-.5,vVectr(end)+.5];
yi = [uVectr(1)-.5,uVectr(end)+.5];

refInfo = geotiffinfo(refFile);
R = refInfo.SpatialRef;
[xlimits,ylimits] = intrinsicToWorld(R,xi,yi);
subR = R;
subR.RasterSize = size(subImage);
subR.XWorldLimits = sort(xlimits);
subR.YWorldLimits = sort(ylimits);

% Write the geotiff
geotiffwrite(outFile,subImage,subR,...
    'GeoKeyDirectoryTag',refInfo.GeoTIFFTags.GeoKeyDirectoryTag);