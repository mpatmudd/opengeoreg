function h = gdb_getGlobalFit(gdbDir,key)
% Read the global fit from the "geodatabase" for the given key

scaleStrMain = gdb_getScaleStrMain(gdbDir);

globFitRoot = fullfile(gdbDir,'data','fit','global',scaleStrMain);
folders = dir(globFitRoot);
folders = folders([folders.isdir]);
folders = folders(arrayfun(@(x) x.name(1), folders) ~= '.');
[~,ind] = sort({folders.date});

globFitDir = fullfile(globFitRoot,folders(ind(end)).name);
fitFile = fullfile(globFitDir,[key,'.mat']);
fitObj = load(fitFile);
h = fitObj.h;
