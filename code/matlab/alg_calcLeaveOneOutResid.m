function Xi = alg_calcLeaveOneOutResid(ijRi,jiRj)
%%
% Description
%   ijRi and jiRj are homogenous matrices representing point matches
%   between image i and image j. For each point n, use the set of other
%   points (~n) to calculate the projective transformation that takes
%   points in j to points in i, Hij(~n). Use Hij(~n) to project the
%   left-out point in image j, jiRj(:,n), to image i:
%
%   jiRi(:,n) = Hij(~n) * jiRj(:,n)
%
%   From this, calculate the residual:
%
%   Xi(1:2,n) = jiRi(1:2,n) - ijRi(1:2,n)
%
%   Sample call(s):
%
%   Xi = alg_calcLeaveOneOutResid(ijRi,jiRj)
% Input(s)
%   Name          Description
%   ijRi          Vector of projective m
%                   Size: 8*numImg x 1
%
%   ijRi_ca       Point matches in image i to image j
%                   Size: 3 x N
%   jiRj_ca       Point matches in image j to image i
%                   Size: 3 x N
%
% Output(s)
%   Xi            Leave-one-out residuals in frame i
%                   Size: 2 x N

N = size(ijRi,2);
Xi = zeros(2,N);
for n = 1:N
    ind = setdiff(1:N,n);
    Hji = alg_solve_two_image_homography(jiRj(:,ind),ijRi(:,ind));
    Hij = alg_invertH(Hji);
    jiRi_n = alg_transform_points_homography(Hij,jiRj(:,n));
    Xi(:,n) = ijRi(1:2,n) - jiRi_n(1:2,:);
end
