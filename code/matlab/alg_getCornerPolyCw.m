function [u,v] = alg_getCornerPolyCw(img,Hri)
% Description
%   Transform the corner pixels of the image into the reference image pixel
%   frame using the projective transformation Hri. Clockwise vertex
%   ordering is enforced on the outputs. Either an image or file pointing
%   to the image can be input.
%
% Input(s)
%   Name          Type      Description
%   img           variable  The image or image file
%   Hri           matrix    Projective transform taking pixels in image
%                          (frame i) to pixels in reference (frame r)
% Output(s)
%   u             numeric   u coordinates of corners
%   v             numeric   v coordinates of corners

% Determine the size of the image without reading the image
if(isnumeric(img))
    % Then this is an image
    N1 = size(img,1);
    N2 = size(img,2);
else
    % Then this is a file containing the image
    info = imfinfo(img);
    N1 = info.Height;
    N2 = info.Width;
end

% Transform the corner points
Ci = [1,1,1; N1,1,1; N1,N2,1; 1,N2,1].';
Cr = alg_transform_points_homography(Hri,Ci);
u = Cr(1,:).';
v = Cr(2,:).';

[u,v] = poly2cw(u,v);