function Rd = alg_transform_points_homography(Hds,Rs)
% Description
%   Transform source points (Rs) to destination points (Rd). Both Rd and Rs
%   are wide (not tall) matrices storing the points in homogenous
%   coordinates.
%
% Sample call(s):
%
%   Rd = alg_transform_points_homography(Hds,Rs)
%
% Input(s)
%   Name        Type                Description
%   Hds         numeric [3 x 3]     The projective matrix that transforms 
%                                   source coordinates to destination
%                                   coordinates
%   Rs          numeric [3 x N]     Homogenous source coordinates
%                                   [xs_1, xs_2, xs_3, ..., xs_N;
%                                    ys_1, ys_2, ys_3, ..., ys_N;
%                                    1   , 1   , 1   , ..., 1   ]
%
% Output(s)
%   Rd          numeric [3 x N]     Homogenous destination coordinates
%                                   [xd_1, xd_2, xd_3, ..., xd_N;
%                                    yd_1, yd_2, yd_3, ..., yd_N;
%                                    1   , 1   , 1   , ..., 1   ]
%

Rd = Hds * Rs;

Rd = Rd ./ repmat(Rd(3,:),3,1);