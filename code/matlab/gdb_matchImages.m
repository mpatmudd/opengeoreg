function gdb_matchImages(gdbDir,scale,surfParam,keys0,keys1)
% keys0 and keys1 are cell arrays of keys with the same lengths,
% representing pairs of images on which to do SURF matching (most likely to
% identify overlap). Iterate over pairs to do the matching.

scaleStr = ['scale',num2str(scale)];

imgDir = fullfile(gdbDir,'img',scaleStr);
surfDir = fullfile(gdbDir,'data','surf',scaleStr);

for ii = 1:length(keys0)
    key0 = keys0{ii};
    key1 = keys1{ii};
    imgName0 = [key0,'.tif'];
    imgName1 = [key1,'.tif'];
    surfMatchData = alg_smartSurfMatch(surfParam,imgName0,imgName1,imgDir,surfDir);
end

gdb_calcOverlapGraph(gdbDir);