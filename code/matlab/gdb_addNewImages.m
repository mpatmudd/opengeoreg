function gdb_addNewImages(gdbDir,imgDir,varargin)
% Add all the images located in the directory imgDir. If copyImage is true
% (the default) the images are copied into the directory */img/original.
% Otherwise, the paths to the images are stored in text files.

if nargin > 2
    copyImages = varargin{1};
else
    copyImages = true;
end

keys = util_dir2keys(imgDir,'extension','tif');

for kk = 1:length(keys)
    fin = fullfile(imgDir,[keys{kk},'.tif']);
    gdb_addNewImage(gdbDir,fin,copyImages);
end
