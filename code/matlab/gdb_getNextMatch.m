function [keyPrev,keyNext] = gdb_getNextMatch(gdbDir,ppParam,surfParam)
% Get the next match for the adapative block registration algorithm. If
% image pairs with known overlap exist, use the pair with the largest
% number of matches. Otherwise, call gdb_getNewLink to get a new pair with
% overlap.

scaleStrMain = ['scale',num2str(ppParam.mainScale)];
scaleStrFast = ['scale',num2str(ppParam.fastScale)];

keysAll = gdb_getAllKeys(gdbDir);

keysFit = gdb_getAlreadyFitKeys(gdbDir);

% Need to check other types of fits, possibly
keysOutsideRef = gdb_getKeysOutsideRef(gdbDir);
keysAll = setdiff(keysAll,keysOutsideRef);

if length(keysFit) == 0
    error('At least one image must be registered. See registerSeedImage');
end

G = gdb_getOverlapGraph(gdbDir);

% Convert indexing of G to integers
nodeNames = G.Nodes.Name;
W = alg_graph2matrix(G);
G = graph(alg_graph2matrix(G,false));
indFit = cell2mat(cellfun(@(s) find(strcmp(s,nodeNames)),keysFit,'UniformOutput',false));
pairInfo = alg_getPotentialPairs(G,indFit);

if size(pairInfo,1) == 0
    %     % Then more SURF matching is needed to connect unregistered images
    [indPrev,indNext] = gdb_getNewLink(gdbDir,W,indFit,keysAll,surfParam,ppParam.fastScale);
    gdb_calcOverlapGraph(gdbDir);
    if ~isnan(indPrev)
        keyPrev = nodeNames{indPrev};
        keyNext = nodeNames{indNext};
    else
        keyPrev = nan;
        keyNext = nan;
    end
else
    [~,pairInd] = max(pairInfo(:,3));
    keyPrev = nodeNames{pairInfo(pairInd,1)};
    keyNext = nodeNames{pairInfo(pairInd,2)};
end