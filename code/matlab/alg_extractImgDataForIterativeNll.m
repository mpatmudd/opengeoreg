function [hi,sigR,sigI,distSqrR_other,distSqrI_other,Rr,Ri,numRefPoints] =...
    alg_extractImgDataForIterativeNll(paramFull,irRi_ca,riRr_ca,ijRi_ca,jiRj_ca,ijRr_ca,jiRr_ca,distSqrR_vect,distSqrI_vect,pairInfo,i)
% Use the current, full solution of the global reprojection problem to
% extract the components needed to maximize the reprojection for a single
% image.

% Create the reduced parameter vector
sigR = paramFull(1);
sigI = paramFull(2);
h = paramFull(3:end);
hi = h((1 + (i-1)*8):(8 + (i-1)*8));
parami = [sigR;sigI;hi];

% Extract the summed total squared distance for all the other matches
numRef = length(irRi_ca);
distSqrR_other = sum(distSqrR_vect(setdiff((1:numRef).',i)));
ind = (pairInfo(:,1) == i) | (pairInfo(:,2) == i);
distSqrI_other = sum(distSqrI_vect(~ind));

% Extract the untransformed homogenous coordinates for frame i (Ri) and the
% transformed homogenous coordinates for the matching frames (r or j, Rr)
numRefPoints = size(riRr_ca{i},2);
ind = find(ind);
Ri = irRi_ca{i};
Rr = riRr_ca{i};
for k = 1:length(ind)
    p = ind(k);
    rev = pairInfo(p,2) == i;
    if ~rev
        Ri = [Ri,ijRi_ca{p}];
        Rr = [Rr,jiRr_ca{p}];
    else
        Ri = [Ri,jiRj_ca{p}];
        Rr = [Rr,ijRr_ca{p}];
    end
end
