function keys = gdb_getAllKeys(gdbDir)
% Get all keys (i.e., those in the original image directory) regardless of
% whether, for example, preprocessing has been done for an image.

origImgDir = fullfile(gdbDir,'img','original');
keys = util_dir2keys(origImgDir,'extension',{'tif';'txt'});