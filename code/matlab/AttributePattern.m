% Description
%   Define a pattern for an attribute used in key mapping. The string of
%   interest can be embedded in four patterns depending on whether there is
%   a preceding and following string. For example, if '5170' is the key
%   field (e.g., an image number) the following patterns are possible:
%
%   '5170'
%   'img5170'
%   '5170rotated
%   'img5170rotated'
%
%   bef = 'img' is the preceding string and aft = 'rotated' is the
%   following string. Constructor:
%
%   pattAttrib = AttributePattern(bef,aft)
%
%   To set bef but not aft (and vice versa) pass an empty string ('') for
%   the other value.

classdef AttributePattern
    properties
        bef='';
        aft='';
    end
    methods
        function obj = AttributePattern(bef,aft)
            obj.bef = bef;
            obj.aft = aft;
        end
        
        function valid = isValid(obj,fullStr)
            valid = startsWith(fullStr,obj.bef) && endsWith(fullStr,obj.aft);
        end
        
        function value = extractValue(obj,fullStr)
            if(~obj.isValid(fullStr))
                error(strcat(fullStr,' is not a valid pattern'));
            end
            value = fullStr(length(obj.bef)+1:end-length(obj.aft));
        end
        
        function pattern = createPattern(obj,value)
            pattern = strcat(obj.bef,value,obj.aft);
        end
        
    end
end