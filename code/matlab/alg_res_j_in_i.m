function resi = alg_res_j_in_i(Hji,ui,vi)
% Calculate the resolution of spatial reference i in j at the point ui/vi.

R0i = [ui;vi;1];
R0j = transform_points_homography(Hji,R0i);
R1j = R0j + [1;1;0];
R1i = transform_points_homography(invertH(Hji),R1j);

dUi = abs(R1i(1) - R0i(1));
dVi = abs(R1i(2) - R0i(2));

resi = mean([dUi,dVi]);