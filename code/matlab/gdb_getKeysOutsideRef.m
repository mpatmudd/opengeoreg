function keys = gdb_getKeysOutsideRef(gdbDir)
% Get a list of keys known to be outside reference. They are stored in the
% file */data/keys_outside_ref.mat

outFile = fullfile(gdbDir,'data','keys_outside_ref.mat');

if ~exist(outFile,'file')
    keys = cell(0,1);
else
    keys = load(outFile);
    keys = keys.keys;
end