function Tds = alg_create_translation_matrix(u0,v0)
% Description
%   Create a translation matrix given the offsets u0 and v0, where the
%   offset is measured in source coordinates (s). The pixel (u0,v0) in
%   source coordinates (s) maps to the pixel (1,1) in destination
%   coordinates (d).
%
% Sample call(s):
%
% Tds = alg_create_translation_matrix(u0,v0)
% 
% Input(s)
%   Name          Description
%   u0            u-offset (first index)
%   v0            v-offset (second index)
%
% Output(s)
%   Tds           Translation matrix mapping source pixels (s) to
%                 destination pixels (d)

Tds = [1,0,1-u0;0,1,1-v0;0,0,1];
