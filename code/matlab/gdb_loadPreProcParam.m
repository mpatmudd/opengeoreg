function ppParam = gdb_loadPreProcParam(gdbDir)
% Load the preprocessing parameters from file in the "geodatabase". They
% are stored in:
%
% */param/preprocessing_parameters.mat

f = fullfile(gdbDir,'param','preprocessing_parameters.mat');
ppParam = load(f,'ppParam');
ppParam = ppParam.ppParam;
