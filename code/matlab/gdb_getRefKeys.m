function keys = gdb_getRefKeys(gdbDir)
% Get the keys (file names) for the reference images. Currently, only one
% reference image is supported by the algorithm, but this function returns
% keys for all images present.

refImgDir = fullfile(gdbDir,'img','ref');
keys = util_dir2keys(refImgDir,'extension',{'tif';'txt'});