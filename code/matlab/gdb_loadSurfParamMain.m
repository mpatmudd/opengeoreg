function surfParamMain = gdb_loadSurfParamMain(gdbDir)
% Load the surf calculation and matching parameters (main scale) from file
% in the "geodatabase". They are stored in:
%
% */param/surf_param_main.mat

f = fullfile(gdbDir,'param','surf_param_main.mat');
surfParamMain = load(f,'surfParamMain');
surfParamMain = surfParamMain.surfParamMain;