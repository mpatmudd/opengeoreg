function H = alg_surfMatchData2HomogMat(surfMatchData)
% Utilize the SURF match data in the input structure, surfMatchData, to
% calculate the projective matrix H.

H = surfMatchData.tform.T.';
% Ensure H is stored as a double and swap first and second axes
H = double(H([2,1,3],[2,1,3]));

% Where should this inversion go? Here for now
H = alg_invertH(H);