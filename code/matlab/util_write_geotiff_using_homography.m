function util_write_geotiff_using_homography(outFile,imgi,Hri,refFile,varargin)
% Description
%   Write a geotiff using the projective matrix (homography) Hri. Hri maps
%   pixels in the image i to pixels in the reference image (r), from which
%   geospatial information is derived.
%
%   This code is based on the folowing Mathworks example:
%   https://www.mathworks.com/help/map/examples/exporting-images-and-raster-grids-to-geotiff.html
%
% Sample call(s):
%
% util_write_geotiff_using_homography(outFile,imgi,Hri,refFile)
% util_write_geotiff_using_homography(outFile,imgi,Hri,refFile,...
%    'outMaskFile',outMaskFile)
%
% Input(s)
%   Name          Description
%   outFile       File for writing the output geotiff
%   imgi          The img to be written
%   Hri           The projective matrix taking image pixels (i/e) to
%                 reference pixels (r)
%   refFile       The reference image file (r)
%   cornersi      [optional] Corner coordinates
%   outMaskFile   [optional] File for writing the output mask as a geotiff
%
% Output(s)
%

% Set up defaults
writeMask = false;
cornersi = [1,1,size(imgi,2),1,1,size(imgi,1),size(imgi,2),size(imgi,1)];

if nargin > 4
    % Read the pairs
    numVarArg = length(varargin);
    if ~mod(numVarArg,2) == 0
        error('Variable inputs must be in variable/value pairs');
    end
    
    for v = 1:(numVarArg/2)
        inputName = varargin{1 + (v-1)*2};
        inputVal = varargin{2 + (v-1)*2};
        
        switch lower(inputName)
            case 'cornersi'
                cornersi = inputVal;
            case 'outmaskfile'
                writeMask = true;
                outMaskFile = inputVal;
            otherwise
                error('Unrecognized variable/value pair')
        end
    end
end

% Find the image corners in the reference image
Ci = [cornersi([2,4,6,8]);cornersi([1,3,5,7]);[1,1,1,1]];
Cr = alg_transform_points_homography(Hri,Ci);

umin = round(min(Cr(1,:)));
umax = round(max(Cr(1,:)));
vmin = round(min(Cr(2,:)));
vmax = round(max(Cr(2,:)));

row = [umin,umax];
col = [vmin,vmax];

% Warp the image to the reference image (using the reference pixel spacing)
if ndims(imgi) > 2
    subImage = repmat(uint8(0),[umax-umin+1,vmax-vmin+1,size(imgi,3)]);
    % Loop over bands
    for bb = 1:size(imgi,3)
        subImage(:,:,bb) = alg_warp_image_homography(Hri,umin:umax,vmin:vmax,imgi(:,:,bb));
    end
else
    subImage = alg_warp_image_homography(Hri,umin:umax,vmin:vmax,imgi);
end


% Determine the geo-spatial information
xi = col + [-.5 .5];
yi = row + [-.5 .5];

refInfo = geotiffinfo(refFile);
R = refInfo.SpatialRef;
[xlimits,ylimits] = intrinsicToWorld(R,xi,yi);
subR = R;
subR.RasterSize = size(subImage);
subR.XWorldLimits = sort(xlimits);
subR.YWorldLimits = sort(ylimits);

% Write the geotiff
geotiffwrite(outFile,subImage,subR,...
    'GeoKeyDirectoryTag',refInfo.GeoTIFFTags.GeoKeyDirectoryTag);

% If necessary, write the mask geotiff
if writeMask
    maski = ones(size(imgi,1),size(imgi,2));
    subMask = alg_warp_image_homography(Hri,umin:umax,vmin:vmax,maski);
    geotiffwrite(outMaskFile,subMask,subR,...
        'GeoKeyDirectoryTag',refInfo.GeoTIFFTags.GeoKeyDirectoryTag);
end
