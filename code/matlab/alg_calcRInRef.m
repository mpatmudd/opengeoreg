function [irRr_ca,ijRr_ca,jiRr_ca] =...
    alg_calcRInRef(h,irRi_ca,ijRi_ca,jiRj_ca,pairInfo,varargin)
%%
% Description
%   Project point matches to the reference frame. This includes both image
%   to reference and image to image correspondences (see
%   alg_extractPointCorrespondences). Points are stored in homogenous
%   coordinates in the cell arrays irRi_ca / riRr_ca [size {numRef x 1}]
%   and ijRi_ca / jiRj_ca [size {numPairs x 1}]. The notation xyRz
%   indicates that the points are for the images x and y and are the
%   matches in image x (yx indicates the corresponding matches in image y).
%   The z to the right of R indicates that the points are projected in
%   frame z.
%
%   All matrices are indexed by integers (instead of, say, image names or
%   hash map keys). The matrix pairInfo [size [numPairs x 1]] gives the
%   index of the first image (i; first column) and second image (j; second
%   column). The vector refInfo (optional) gives the indices of images with
%   image to reference images. If refInfo is not input, it is assumed to be
%   1, 2, 3, ..., length(irRi_ca).
%
%   Note: numImg is the number of images in the dataset whereas numRef is
%   the number of image to reference matches. If refInfo is not input,
%   these should be the same.
%
%   Sample call(s):
%
%   [irRr_ca,ijRr_ca,jiRr_ca] =...
%    alg_calcRInRef(h,irRi_ca,ijRi_ca,jiRj_ca,pairInfo)
%
%   [irRr_ca,ijRr_ca,jiRr_ca] =...
%    alg_calcRInRef(h,irRi_ca,ijRi_ca,jiRj_ca,pairInfo,refInfo)
    
% Input(s)
%   Name          Description
%   h             Vector of projective matrices
%                   Size: 8*numImg x 1
%
%   irRi_ca       Cell array of point matches in i to the reference image r
%                   Size: numRef x 1
%
%   riRr_ca       Cell array of point matches in the reference image to i
%                   Size: numRef x 1
%
%   ijRi_ca       Cell array of point matches in i to j
%                   Size: numPairs x 1
%
%   jiRj_ca       Cell array of point matches in j to i
%                   Size: numPairs x 1
%
%   pairInfo      Matrix mapping pair index to image indices for image to
%                 image pairs. First column gives i for the pair and second
%                 column j for the pair
%                   Size: numPairs x 2
%
% Output(s)
%   irRi_ca       Cell array of point matches in i to the reference image r
%                 in the reference frame
%                   Size: numRef x 1
%   ijRr_ca       Cell array of point matches in i to j in the reference
%                 frame
%                   Size: numPairs x 1
%   jiRr_ca       Cell array of point matches in j to i in the reference
%                 frame
%                   Size: numPairs x 1

numRef = length(irRi_ca); % Number of image to reference matches

if nargin > 5
    refInfo = varargin{1};
else
    refInfo = (1:numRef).';
end

numPairs = length(ijRi_ca); % Number of image to image pairs

% Loop over image to reference matches to calculate irRr
irRr_ca = cell(numRef,1);
parfor m = 1:numRef
    i = refInfo(m);
    Hri = alg_homogVect2Mat(h((1 + (i-1)*8):(8 + (i-1)*8)));
    irRr = alg_transform_points_homography(Hri,irRi_ca{m});
    irRr_ca{m} = irRr;
end

% Loop over image to image pairs to calculate ijRr and jiRr
ijRr_ca = cell(numPairs,1);
jiRr_ca = cell(numPairs,1);
parfor p = 1:numPairs
    i = pairInfo(p,1);
    j = pairInfo(p,2);
    Hri = alg_homogVect2Mat(h((1 + (i-1)*8):(8 + (i-1)*8)));
    Hrj = alg_homogVect2Mat(h((1 + (j-1)*8):(8 + (j-1)*8)));
    ijRr = alg_transform_points_homography(Hri,ijRi_ca{p});
    jiRr = alg_transform_points_homography(Hrj,jiRj_ca{p});
    ijRr_ca{p} = ijRr;
    jiRr_ca{p} = jiRr;
end