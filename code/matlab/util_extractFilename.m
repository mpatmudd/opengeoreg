function output = util_extractFilename(input)
% Extract the file name or file names. Three types of inputs are supported:
%
% (1) string           extract and return the file name (a string is
%                      returned)
% (2) ImageDatastore
%     OR
% (3) cell array       extract and return the file name for all files (a
%                      cell array is returned)
%

if ischar(input)
    [~,name,ext] = fileparts(input);
    output = [name,ext];
elseif isa(input,'matlab.io.datastore.ImageDatastore')
    output = util_extractFilename(input.Files);
elseif iscell(input)
    output = cellfun(@util_extractFilename,input,'UniformOutput',false);
else
    error('Unrecognized input class');
end


