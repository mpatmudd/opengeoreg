function gdb_addRefImage(gdbDir,inputFile,varargin)
% Add the input reference image to the "geodatabase". It is stored in:
%
% */img/ref
%
% Depending on the optional input copyImg (default false) it is either
% copied into the directory or the path is stored in a text file.

if nargin > 2
    copyImg = varargin{1};
else
    copyImg = false;
end

[~,key,ext] = fileparts(inputFile);
if copyImg
    fOut = fullfile(gdbDir,'img','ref',[key,ext]);
    copyfile(inputFile,fOut);
else
    fOut = fullfile(gdbDir,'img','ref',[key,'.txt']);
    fid = fopen(fOut,'wt');
    fprintf(fid, '%s\n',inputFile);
    fclose(fid);
end