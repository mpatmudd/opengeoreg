function gdb_addKeyOutsideRef(gdbDir,key)
% Add the input key to the list of keys outside the reference

outFile = fullfile(gdbDir,'data','keys_outside_ref.mat');

if ~exist(outFile,'file')
    keys = {key};
    save(outFile,'keys');
else
    keys = load(outFile);
    keys = keys.keys;
    keys = unique([keys;key]);
    save(outFile,'keys');
end

gdb_calcOverlapGraph(gdbDir);