function hVect = alg_homogMat2Vect(Hmat)
% Description
%   Convert a projective matrix to vector representation (eight degrees of
%   freedom). Ordering follows Matlab's : operator.
%
% Sample call(s):
%
% hVect = alg_homogMat2Vect(Hmat)
%
% Input(s)
%   Name          Description
%   Hmat          Projective matrix to convert
%
% Output(s)
%   hVect         Vector representation of projective matrix

% Ensure that the (3,3) element equals 1
Hmat = Hmat / Hmat(3,3);

% Convert to vector and extract 8 unique elements
hVect = Hmat(:);
hVect = hVect(1:8);
