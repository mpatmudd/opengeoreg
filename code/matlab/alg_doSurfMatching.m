 function [H01,numMatches,inlier0,inlier1] = alg_doSurfMatching(img0,img1,...
    varargin)
% Description
%   Utilize SURF keypoints to find matching features that are in both img0
%   and img1 using the standard approach in computer vision:
%
%   (1) Calculate features
%   (2) Match features
%   (3) Identify outliers [given a projective transformation]
%
% Input(s)
%   Name          Type      Description
%   img0          image     The first image
%   img1          image     The second image
%   thresh        numeric   Metric Threshold for SURF feature detection
%   numPoints     interger  Number of points to use in matching
%   msacRadius    numeric   Radius (Max Distance) for rejection of outliers
%   msacTrials    integer   Maximum number of trials for outlier detection
%
% Output(s)
%   H01           matrix    [3 x 3] projective matrix taking pixels in frame
%                           1 to pixels in frame 0
%   numMatches    integer   Number of matches after outlier detection
%   inlier0       double    [numMatches x 1] Vector of inlier matches in
%                           img0
%   inlier1       double    [numMatches x 1] Vector of inlier matches in
%                           img1

% (0) Handle inputs
% Varargin is in pairs of specification and value
% Set up defaults

thresh = 100;
numPoints = -1;
msacRadius = 10;
msacTrials = 10000;
useEst = false;
% H01_est = eye(3);

if nargin > 2
    % Read the pairs
    numVarArg = length(varargin);
    if ~mod(numVarArg,2) == 0
        error('Variable inputs must be in variable/value pairs');
    end
    
    for v = 1:(numVarArg/2)
        inputName = varargin{1 + (v-1)*2};
        inputVal = varargin{2 + (v-1)*2};
        
        switch lower(inputName)
            case 'thresh'
                thresh = inputVal;
            case 'numpoints'
                numPoints = inputVal;
            case 'msacradius'
                msacRadius = inputVal;
            case 'msactrials'
                msacTrials = inputVal;
            case 'h01_est'
                useEst = true;
                H01_est = inputVal;
            otherwise
                error('Unrecognized variable/value pair')
        end
        
    end
end

% (1) Calculate features
% If an initial estimate of the transform is given, only use points inside
% the intersection of the two images. To accomplish, create the region of
% interest (roi) for the overlap in both images by calling
% alg_calcOverlapRoi.
if useEst
    [roi0,roi1] = alg_calcOverlapRoi(img0,img1,H01_est);
    points0 = detectSURFFeatures(img0,'MetricThreshold',thresh,'ROI',roi0);
    points1 = detectSURFFeatures(img1,'MetricThreshold',thresh,'ROI',roi1);
else
    points0 = detectSURFFeatures(img0,'MetricThreshold',thresh);
    points1 = detectSURFFeatures(img1,'MetricThreshold',thresh);
end




if numPoints > 0
    [features0,validPoints0] = extractFeatures(img0,points0.selectStrongest(numPoints));
    [features1,validPoints1] = extractFeatures(img1,points1.selectStrongest(numPoints));
else
    [features0,validPoints0] = extractFeatures(img0,points0);
    [features1,validPoints1] = extractFeatures(img1,points1);
end

% (2) Match features
indexPairs = matchFeatures(features0,features1,'Method','Approximate');
matchedPoints0 = validPoints0(indexPairs(:,1));
matchedPoints1 = validPoints1(indexPairs(:,2));

% (3) Detect outliers
% [tform, inlier0, inlier1] = estimateGeometricTransform(matchedPoints0, matchedPoints1, 'projective','MaxDistance',radius,'MaxNumTrials',10000);
try
    [tform, inlier1, inlier0] = estimateGeometricTransform(matchedPoints1,...
        matchedPoints0, 'projective','MaxDistance',msacRadius,...
        'MaxNumTrials',msacTrials);
    numMatches = length(inlier0);
    H01 = tform.T.';
    % For some reason Matlab stores SURF points as singles, but later functions want
    % it as doubles.
    H01 = double(H01([2,1,3],[2,1,3]));
catch exception
    numMatches = 0;
    H01 = nan(3,3);
    inlier0 = nan;
    inlier1 = nan;
end