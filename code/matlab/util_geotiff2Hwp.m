function Hwp = util_geotiff2Hwp(geotiffFile)
% Return the projective matrix (typically affine) that takes pixels to
% world coordinates.
%
% p is pixel coordinates (u/v)
% w is world coordinates
%
% It is unclear to me whether this generalizes to any geotiff as I am
% unsure of all of Matlab's conventions for reading geotiff information.
%
% The equations are:
%
% xw = Xmin + dX * (v - 0.5)
% y2 = Ymax - dY * (u - 0.5)
%
% where
%
% Xmin = XWorldLimits(1)
% Ymax = YWorldLimits(2)
% dX   = CellExtentInWorldX
% dY   = CellExtentInWorldY

geoInfo = geotiffinfo(geotiffFile);

Xmin = geoInfo.SpatialRef.XWorldLimits(1);
Ymax = geoInfo.SpatialRef.YWorldLimits(2);
if sum(strcmp(fieldnames(geoInfo.SpatialRef), 'CellExtentInWorldX')) > 0
    dX   = geoInfo.SpatialRef.CellExtentInWorldX;
    dY   = geoInfo.SpatialRef.CellExtentInWorldY;
else
    dX = geoInfo.PixelScale(1);
    dY = geoInfo.PixelScale(2);
end

Hwp = [0, dX, Xmin - dX/2; -dY, 0, Ymax + dY/2; 0, 0, 1];