function keys = gdb_getPreProcKeysMain(gdbDir)
% Get the keys of images that have been preprocessed by identifying .tif
% files in the following directory (main scale):
%
% */img/scaleStrMain

scaleStrMain = gdb_getScaleStrMain(gdbDir);


keys = util_dir2keys(fullfile(gdbDir,'img',scaleStrMain),...
    'extension','tif');

keys = setdiff(keys,util_filterStart(keys,'mask_'));