function gdb_saveSurfParamFast(gdbDir,surfParamFast)
% Save the input surf calculation and matching parameters (fast scale) to
% the "geodatabase". The paramaters are stored in the following file:
%
% */param/surf_param_fast.mat

f = fullfile(gdbDir,'param','surf_param_fast.mat');
save(f,'surfParamFast');
