function [umat,vmat] = alg_arcImgCoordToMatlabPixels(xarc,yarc,Nu,dpi)
% Description
%   Transform image coordinates (xarc and yarc) as represented by ArcMAP to
%   pixel coordinates. ArcMAP utilizes a pixel spacing (dpi) and inverts
%   the first axis relative to the Matlab. In addition, ArcMAP utilizes a
%   convention whereby pixel indices correspond to the corner of the pixel,
%   whereas Matlab utilizes a convention whereby pixel indies correspond to
%   the center of the pixel (hence the 0.5 shift).
%
% Sample call(s):
%
% [umat,vmat] = alg_arcImgCoordToMatlabPixels(xarc,yarc,Nu,dpi)
% 
% Input(s)
%   Name          Description
%   xarc          Vector of ArcMAP image coordinates (first index)
%   yarc          Vector of ArcMAP image coordinates (second index)
%   Nu            Image size for the first index
%   dpi           Pixel spacing detected or assumed by ArcMAP in original
%                 image. This is not stored with the geo-referencing points
%                 saved by ArcMAP to an external text file. dpi stands for
%                 dots per inch, but this could be in another distance
%                 unit, too (e.g., meters or feet).
%
% Output(s)
%   umat          Vector of pixel coordinates using Matlab's pixel ordering
%                 convention (first axis)
%   vmat          Vector of pixel coordinates using Matlab's pixel ordering
%                 convention (second axis)
umat = (Nu-dpi*yarc) + 0.5;
vmat = dpi*xarc + 0.5;
