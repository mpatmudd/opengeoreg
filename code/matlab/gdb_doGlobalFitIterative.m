function [keys,paramCa,etaVect] = gdb_doGlobalFitIterative(gdbDir,scale)
%%
% Description
%   Minimize reprojection errors of image-to-image and image-to-reference
%   matches in the reference frame. This is done interatively by cycling
%   over all the images multiple times. The result is a global refinement
%   of all the image to reference projective transformations. The error
%   processes for the magnitude of the image-to-image and image-to-reference
%   matches are assumed to be Rayleigh, but with different
%   parameterizations (i.e., different widths of the probability density
%   functions, which come from the standard deviation of the underlying 2D
%   normal distributions).
%
%   Sample call(s):
%
% Input(s)
%   Name          Description
%   gdbDir        Path to the geodatabase directory
%   scale         Scale to use
%
% Output(s)
%   keys          Cell array of keys; the entries in paramCa maintains the
%                 ordering
%   paramCa       Cell array of parameters (fits) for each iteration. Each
%                 entry is a vector containing [sigR;sigI;h], where sigR
%                 is the standard deviation of the image-to-reference
%                 error process (Rayleigh), sigI is the standard deviation
%                 of the image-to-image error process (Rayleigh), and h is
%                 the vector of projective transformations. h has
%                 dimensions [length(keys)*8 x 1].
%   etaVect       Vector of values of the negative log-likelihood after
%                 to start with and after each iteration over all the
%                 images.

scaleStr = ['scale',num2str(scale)];

fitDir = fullfile(gdbDir,'data','fit','local',scaleStr);
imgDir = fullfile(gdbDir,'img',scaleStr);

[fitHash,regDataHash] = util_dir2fitHash(fitDir);

[refTiesHash,pairTiesHash,hHash,~,Wglob,keys]...
    = alg_extractPointCorrespondences(fitHash,imgDir,regDataHash);

[h0,irRi_ca,riRr_ca,ijRi_ca,jiRj_ca,pairInfo] =...
    alg_pointCorr2IndexRep(refTiesHash,pairTiesHash,hHash,keys,Wglob);

[irD20,ijD20] =...
    alg_calcDistSqrInRef(h0,irRi_ca,riRr_ca,ijRi_ca,jiRj_ca,pairInfo);
rayr = fitdist(sqrt(irD20).','rayleigh');
rayi = fitdist(sqrt(ijD20).','rayleigh');
% Standard deviation of matches for the block registrations
sigR = rayr.B;
% Standard deviation of matches for the SURF keypoint matches
sigI = double(rayi.B);

param0 = [sigR;sigI;h0];



% The objective function to use for the maximum likelihood estimation
objFun = @(p) alg_calcNllGlobal(p,irRi_ca,riRr_ca,ijRi_ca,jiRj_ca,pairInfo);
% Define mleOptions to plot the progress of the maximum likelihood
% estimation. Default number of function evaluations and iterations:
mleOptions = optimset('PlotFcns',@optimplotfval,...
    'MaxIter',200000,'MaxFunEvals',200000);


numIter = 10;
paramCa = cell(numIter+1,1);
paramCa{1} = param0;
etaVect = nan(numIter+1,1);
etaVect(1) = alg_calcNllGlobal(param0,irRi_ca,riRr_ca,ijRi_ca,jiRj_ca,pairInfo);
for ii = 1:numIter
    disp(['Iteration ', num2str(ii)]);
    tic;
    paramCa{ii+1} = alg_doOneIterationOfGlobalFit(paramCa{ii},irRi_ca,riRr_ca,ijRi_ca,jiRj_ca,pairInfo);
    etaVect(ii+1) = alg_calcNllGlobal(paramCa{ii+1},irRi_ca,riRr_ca,ijRi_ca,jiRj_ca,pairInfo);
    toc;
end