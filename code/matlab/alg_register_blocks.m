function [affine_ca,affine0_ca,Rmov,Rfix] =...
    alg_register_blocks(M,F,Tep,umin,umax,vmin,vmax,varargin)
%%
% Description
%   Loop over blocks and register them using an affine transformation
%
% Sample call(s):
%
%
%
% Input(s)
%   Name          Description
%   M             The moving image
%   F             The fixed image (to be registered; blocks drawn from F)
%   Tep           Translation matrix taking padded points in the
%                 transformed reference image (p) to errored image pixels
%                 (e)
%   umin          | umin, umax, vmin, and vmax are vectors of the same
%   umax          | length giving the extents of the blocks to be used.
%   vmin          |
%   vmax          |
%   optimizer     [optional] Optimizer object for the call to imregtform.
%
% Output(s)
%   affine_ca     Cell array of affine transformation matrices for each
%                 block after block registration
%   affine0_ca    Cell array of affine transformation matrices for each
%                 block prior to block registration (i.e., initial
%                 estimates)
%   Rmov          Center points of blocks in the moving frame calculated
%                 by transforming Rfix into the moving frame using the
%                 affine block registrations
%   Rfix          Center points of blocks in the fixed frame
%

%% Handle variable inputs
if nargin > 7 
    optimizer = varargin{1};
else
    [optimizer, metric] = imregconfig('multimodal');
    optimizer.InitialRadius = optimizer.InitialRadius/10;
    optimizer.MaximumIterations = 100;
end

%% Initialize variables
numBlocks = length(umin);
affine_ca = cell(numBlocks,1);
affine0_ca = cell(numBlocks,1);
Rmov = ones(3,numBlocks);
Rfix = ones(3,numBlocks);

%%
% Loop over blocks to do registrations. If supported, parallel processing
% is used by using parfor instead of for.
parfor n = 1:numBlocks
% for n = 1:numBlocks
    % Define initial transformation and subset block
    Tblk = alg_create_translation_matrix(umin(n),vmin(n));
    Ttot = Tblk * Tep;
    affine0 = affine2d(Ttot([2,1,3],[2,1,3]).');
    affine0_ca{n} = affine0;
    B = alg_subsetByTransMatrix(F,Tblk,umax(n)-umin(n)+1,vmax(n)-vmin(n)+1);
    
    affine = imregtform(M,B,'affine',optimizer,metric,'InitialTransform',affine0);
    
    affine_ca{n} = affine;
    
    An = affine.T.'; % Axes swapped with respect to indexing
    An = An([2,1,3],[2,1,3]);
    Hfm = alg_invertH(Tblk) * An; % Update estimate
    Hmf = alg_invertH(Hfm); % i.e., Hpi
    
    % Calculate block centers
    Rfix(:,n) = [(umin(n)+umax(n))/2;(vmin(n)+vmax(n))/2;1];
    Rmov(:,n) = Hmf * Rfix(:,n);
    
%     M_in_B = imwarp(M,affine,'OutputView',imref2d(size(B)));
%     M_in_B0 = imwarp(M,affine0,'OutputView',imref2d(size(B)));
%     J0 = imfuse(B,M_in_B0);
%     J = imfuse(B,M_in_B);
%     figure(1);
%     subplot(1,2,1);
%     imshow(J0);
%     subplot(1,2,2);
%     imshow(J);
end