function surfParamMain = gdb_getDefaultSurfParamMain
% Description
%   Return default parameters for the SURF keypoint identification and
%   matching [main scale]
% Input(s)
%   Name              Description
%   [none]
%
% Output(s)
%   surfParamMain     Structure containing the following fields:
%     thresh          Metric threshold for call to detectSURFFeatures
%     radius          Max distance for call to estimateGeometricTransform
%     maxTrials       Maximum number of trials in call to
%                     estimateGeometricTransform
%     minMatch        Minimum number of matching points required to acccept
%                     that two images overlap

surfParamMain.thresh = 10;
surfParamMain.radius = 10;
surfParamMain.maxTrials = 10000;
surfParamMain.minMatch = 40;