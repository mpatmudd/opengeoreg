function param = alg_doOneIterationOfGlobalFit(param0,irRi_ca,riRr_ca,ijRi_ca,jiRj_ca,pairInfo)
% Loop once over all images. For each image, minimize the re-projection
% error. For further details see gdb_doGlobalFitIterative and
% alg_calcRInRef.

h0 = param0(3:end);

[irRr_ca,ijRr_ca,jiRr_ca] = alg_calcRInRef(h0,irRi_ca,ijRi_ca,jiRj_ca,pairInfo);
[distSqrR_vect,distSqrI_vect,Nr,Ni] = alg_calcDistSqrFromR(irRr_ca,riRr_ca,ijRr_ca,jiRr_ca);

mleOptions = optimset('PlotFcns',@optimplotfval,...
    'MaxIter',200000,'MaxFunEvals',200000);

h = h0;
numImg = length(irRi_ca);
param = param0;
for i = 1:numImg
    [hi,sigR,sigI,distSqrR_other,distSqrI_other,Rr,Ri,numRefPoints] = alg_extractImgDataForIterativeNll(param,irRi_ca,riRr_ca,ijRi_ca,jiRj_ca,ijRr_ca,jiRr_ca,distSqrR_vect,distSqrI_vect,pairInfo,i);
    
    objFunIter = @(p) alg_calcReprojNllForOneImage(p,sigR,sigI,Nr,Ni,distSqrR_other,distSqrI_other,Rr,Ri,numRefPoints);
    parami = fminsearch(objFunIter,hi,mleOptions);
    [param,irRr_ca,ijRr_ca,jiRr_ca,distSqrR_vect,distSqrI_vect] =...
        alg_updateIterativeNllSoln(param,parami,pairInfo,i,irRi_ca,riRr_ca,ijRi_ca,jiRj_ca,irRr_ca,ijRr_ca,jiRr_ca,distSqrR_vect,distSqrI_vect,Nr,Ni);
end

