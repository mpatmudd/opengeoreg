function [fiti,varargout] = alg_adaptive_block_registration(imgi,ref,...
    Hre,blockSize,blockSpacing,radius,padding)
%%
% Description
%   Register the input image (i) to the reference image by independently
%   registering sub-blocks of the unregistered image, then identifying and
%   rejecting bad sub-registrations. This is done in two primary steps:
%
%   Block Registrations
%     Use mutual information maximization to independently register
%     sub-images of image i (unregistered) to the reference image (r)
%   Block Outlier Detection
%     Identify bad block registerations using the MSAC algorithm
%
%   Sample call(s):
%
% fiti = alg_adaptive_block_registration(imgi,ref,Hre,...
%    blockSize,blockSpacing,radius,padding)
%
% [fiti,regData] = alg_adaptive_block_registration(imgi,ref,Hre,...
%    blockSize,blockSpacing,radius,padding)

% Input(s)
%   Name          Description
%   imgi          The image to be registered indexed by i/e, where e (for
%                 epsilon) is the "errored" image frame associated with i
%   ref           The reference image or referece image file name
%   Hre           The initial estimate of the projective transformation
%                 taking pixels in image i to the reference frame (r). e is
%                 used instead of i because this is the "errored" frame
%                 that needs to be corrected.
%   blockSize     The size of the sub-blocks
%   blockSpacing  The spacing between sub-blocks
%   radius        The radius (or maximum distance) to use for the outlier
%                 detection with the MSAC algorithm
%   padding       The padding proportion to use for transforming the
%                 reference image (r) into the image (i/e) pixel spacing
%                 prior to the block registrations.
%
% Output(s)
%   fiti          A structure summarizing the fit
%   regData       A cell array storing the block registration data

%% (a) Transform the reference image into the unregistered image's pixel
%      spacing.
%
% Tsr (a translation) takes points in the full reference image to
% a sub-set of the reference image (hence s) and Tep (a translation) takes
% points in the transformed, padded reference image (p) to the errored
% image frame:
%
% Hre = Trs Hsp Tpe
%
% where reversed indices indicate matrix inverses.
[refSubInImg,Tsr,Tep,padLim,refSubLim] =...
    alg_placeRefInImg(ref,imgi,Hre,padding);
%% (b) Define the block extents
[umin,vmin] = meshgrid(1:blockSpacing:(size(imgi,1)-blockSize),...
    1:blockSpacing:(size(imgi,2)-blockSize));
umin = umin(:);
vmin = vmin(:);
umax = umin + blockSize - 1;
vmax = vmin + blockSize - 1;

%% (c) Do the block registrations
[affine_ca,affine0_ca,Rmov,Rfix] =...
    alg_register_blocks(refSubInImg,imgi,Tep,umin,umax,vmin,vmax);

%% (d) Do the outlier detection and update Hre to derive Hri
[homog,inlierMov,inlierFix] =...
    estimateGeometricTransform(Rmov(1:2,:).',Rfix(1:2,:).',...
    'projective','MaxDistance',radius,'MaxNumTrials',10000);
Hip = homog.T.';

Hpi = alg_invertH(Hip);
Hri = Hre * Tep * Hpi;

Hri = Hri / Hri(3,3);

fiti = struct('H0',Hre,'Hblk',Hri,'Tsr',Tsr,'Tep',Tep);

%% (e) If necessary, return registration data
if nargout > 1
    Rref = alg_transform_points_homography(Hre * Tep,Rmov);
    regData = {affine_ca,affine0_ca,Tsr,Tep,padLim,refSubLim,...
        Rmov,Rfix,inlierMov,inlierFix,Rref};
    varargout{1} = regData;
end