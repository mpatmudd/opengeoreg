function vis_displayFramingResult(edgeFitData,I,varargin)
% Create a figure summarizing a framing operation. The image I is plotted
% and data in the structure edgeFitData is used to display the framing,
% including the points used (green) and not used (red) in the edge fits.

if nargin > 2
    figure(varargin{1});
else
    figure;
end

S = size(I);
imshow(I);

slp_upper = -edgeFitData.fit_upper.Theta(1)/edgeFitData.fit_upper.Theta(2);
int_upper = -edgeFitData.fit_upper.Theta(3)/edgeFitData.fit_upper.Theta(2);

slp_lower = -edgeFitData.fit_lower.Theta(1)/edgeFitData.fit_lower.Theta(2);
int_lower = -edgeFitData.fit_lower.Theta(3)/edgeFitData.fit_lower.Theta(2);

slp_left = -edgeFitData.fit_left.Theta(1)/edgeFitData.fit_left.Theta(2);
int_left = -edgeFitData.fit_left.Theta(3)/edgeFitData.fit_left.Theta(2);

slp_right = -edgeFitData.fit_right.Theta(1)/edgeFitData.fit_right.Theta(2);
int_right = -edgeFitData.fit_right.Theta(3)/edgeFitData.fit_right.Theta(2);

[ulcx,ulcy] = alg_find_line_intersection(slp_upper,int_upper,slp_left,int_left);
[llcx,llcy] = alg_find_line_intersection(slp_lower,int_lower,slp_left,int_left);
[lrcx,lrcy] = alg_find_line_intersection(slp_lower,int_lower,slp_right,int_right);
[urcx,urcy] = alg_find_line_intersection(slp_upper,int_upper,slp_right,int_right);

upper_left  = [ulcx,ulcy];
lower_left  = [llcx,llcy];
lower_right = [lrcx,lrcy];
upper_right = [urcx,urcy];

ind_upper = edgeFitData.fit_upper.CS;
ind_lower = edgeFitData.fit_lower.CS;
ind_left = edgeFitData.fit_left.CS;
ind_right = edgeFitData.fit_right.CS;

hold on;
plot(edgeFitData.Dupper(1,  ind_upper),edgeFitData.Dupper(2,  ind_upper),'.g')
plot(edgeFitData.Dupper(1, ~ind_upper),edgeFitData.Dupper(2, ~ind_upper),'.r')
x0_upper = 1;
x1_upper = S(2);
y0_upper = slp_upper*x0_upper + int_upper;
y1_upper = slp_upper*x1_upper + int_upper;
plot([x0_upper,x1_upper],[y0_upper,y1_upper],'y');

plot(edgeFitData.Dlower(1,  ind_lower),edgeFitData.Dlower(2,  ind_lower),'.g')
plot(edgeFitData.Dlower(1, ~ind_lower),edgeFitData.Dlower(2, ~ind_lower),'.r')
x0_lower = 1;
x1_lower = S(2);
y0_lower = slp_lower*x0_lower + int_lower;
y1_lower = slp_lower*x1_lower + int_lower;
plot([x0_lower,x1_lower],[y0_lower,y1_lower],'y');

plot(edgeFitData.Dleft(2,  ind_left),edgeFitData.Dleft(1,  ind_left),'.g')
plot(edgeFitData.Dleft(2, ~ind_left),edgeFitData.Dleft(1, ~ind_left),'.r')
y0_left = 1;
y1_left = S(1);
x0_left = slp_left*y0_left + int_left;
x1_left = slp_left*y1_left + int_left;
plot([x0_left,x1_left],[y0_left,y1_left],'y');

plot(edgeFitData.Dright(2,  ind_right),edgeFitData.Dright(1,  ind_right),'.g')
plot(edgeFitData.Dright(2, ~ind_right),edgeFitData.Dright(1, ~ind_right),'.r')
y0_right = 1;
y1_right = S(1);
x0_right = slp_right*y0_right + int_right;
x1_right = slp_right*y1_right + int_right;
plot([x0_right,x1_right],[y0_right,y1_right],'y');

meanx = (upper_left(1) + lower_left(1) + lower_right(1) + upper_right(1))/4;
meany = (upper_left(2) + lower_left(2) + lower_right(2) + upper_right(2))/4;
plot(meanx,meany,'r+');