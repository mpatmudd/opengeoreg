function gdb_addNewImage(gdbDir,fin,varargin)
% Add the image located at fin (path to the file). If copyImage is true
% (the default) the image is copied into the directory */img/original.
% Otherwise, the path to the image is stored in a text file.

if nargin > 2
    copyImage = varargin{1};
else
    copyImage = true;
end

origImgDir = fullfile(gdbDir,'img','original');
if copyImage
    copyfile(fin,origImgDir);
else
    [~,name,ext] = fileparts(fin);
    fOut = fullfile(origImgDir,[name,'.txt']);
    fid = fopen(fOut,'wt');
    fprintf(fid, '%s\n',fin);
    fclose(fid);
end
