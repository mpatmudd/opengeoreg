function [umat,vmat] = alg_arcGeoCoordToMatlabPixels(xgeo,ygeo,geoFile)
% Description
%   Transform geo-coordinates (xgeo and ygeo) as represented by ArcMAP to
%   pixel coordinates.
%
% Sample call(s):
%
% [umat,vmat] = alg_arcGeoCoordToMatlabPixels(xgeo,ygeo,geoFile)
% 
% Input(s)
%   Name          Description
%   xgeo          Vector of geo-coordinates (first index)
%   ygeo          Vector of geo-coordinates (second index)
%   geoFile       The geotiff file containing the coordinate system
%                 information
%
% Output(s)
%   umat          Vector of pixel coordinates using Matlab's pixel ordering
%                 convention (first axis)
%   vmat          Vector of pixel coordinates using Matlab's pixel ordering
%                 convention (second axis)
geoInfo = geotiffinfo(geoFile);
[umat,vmat] = map2pix(geoInfo.SpatialRef,xgeo,ygeo);