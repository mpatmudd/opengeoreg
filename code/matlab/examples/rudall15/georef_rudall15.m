%% Overview
% This is the main script for the rudall15 Matlab example of the free
% automated image georeferencing tool, openGeoReg. Rudall is a geographic
% area in Australia's Western Desert defined by Geoscience Australia. The
% images used this example are historic aerial images captured in the
% 1950s. 15 digitized image negatives are used -- hence the name of the
% example, rudall15.

%% Setup
% Clear the workspace and close any open figures
clear;
close all;

% Specify paths
% baseDir is the base project directory. To begin, it should only have an
% \inputs directory with the following sub-directories:
%
% *\inputs\aerial  --  The 15 historic aerial images, to be geo-referenced
% *\inputs\ref     --  The reference image, already geo-referenced
% *\inputs\seed    --  A seed image that overlaps one of the aerial images,
%                      already geo-referenced
%
% Below, the function gdb_createGdbDir creates a new directory, gdb (the
% "geodatabase"), which consists of the following sub-directories:
%
% *\gdb\data       --  Intermediate and final algorithm results (exlcuding
%                      images)
% *\gdb\img        --  Images (or links to images) from all stages of the
%                      processing
% *\gdb\param      --  Parameters that control algorithm behavior
%
% By reading and writing all information to a "geodatabase," processing can
% be paused or interupted and results can be examined at all stages of
% processing. For example, the function gdb_displayFramingResult summarizes
% the framing result from the pre-processing state. If desired, all the
% framing of all the images can be checked before proceeding to the main
% geo-referencing.
%
% The base directory will need to be changed. The input data is
% available for download from:
%
% https://www.dropbox.com/sh/2wqxso6r7iev2q5/AAAbYSpKXWlmIic87En39_uUa?dl=0
baseDir = 'C:\Users\mhp12\rudall15';
inputDir = fullfile(baseDir,'inputs');

if ~exist(inputDir,'dir')
    error('Could not find input data folder in base directory')
end

% Create the geodatabase. If it already exists, it will not be overwritten
% unless overwriteGdb is set to true on the next line
overwriteGdb = false;
gdbDir = fullfile(baseDir,'gdb');
if overwriteGdb && exist(gdbDir,'dir')
        error('The geodatabase already exists but overwriteGdb is false.')
end

% Modify the default SURF keypoint identification and matching parameters
surfParamFast = gdb_getDefaultSurfParamFast;
surfParamFast.minMatch = 20;
surfParamMain = gdb_getDefaultSurfParamMain;
surfParamMain.minMatch = 50;

% Create the geodatabase. Algorithm parameters are stored in the direcotry
% */gdb/param
gdb_createGdbDir(gdbDir,'ReplaceDir',overwriteGdb,...
    'surfParamMain',surfParamMain,'surfParamFast',surfParamFast,...
    'createMaxResDir',true);

% Add the reference image, which is stored in the directory */gdb/img/ref.
% Since the third input in gdb_addRefImage is false in the call below, the
% path to the reference image is stored in a .txt file in */gdb/img/ref. To
% instead copy the reference image into the geodatabase, change the flag to
% true.
refFile = fullfile(baseDir,'inputs','ref',...
    'AGRI_clip_rudall15.tif');
gdb_addRefImage(gdbDir,refFile,false);

% Add the seed image, which is stored in the directory */gdb/img/seed.
% Unlike the reference image, the seed image is always copied into the
% geodatabase; however, the function could be updated to support .txt paths
% as with the reference image and original aerial images. The seed image is
% any geo-referenced image that overlaps one of the un-geo-referenced
% aerial images. It is used to initialize the geometry of the first aerial
% image that is processed using keypoint matching. One way to create a seed
% image is to geo-reference one of the aerial images using Quantum GIS.
% ArcMap *may* also work, but I [MHP] have experienced problems with seed
% images geo-referenced in ArcMap.
keySeed = '31484183_5141';
seedFile = fullfile(baseDir,'inputs','seed',['seed',keySeed,'.tif']);
gdb_addSeedImage(gdbDir,keySeed,seedFile);

% Add the un-geo-referenced aerial images, which are stored in the
% directory */gdb/img/seed/original. As with the reference image, the
% original images can either be copied or linked to as paths in .txt files.
% To copy the images instead of storing the paths set the final input on
% the following line to true.
gdb_addNewImages(gdbDir,fullfile(baseDir,'inputs','aerial'),false);

% Preprocess all the images. The resulting, preprocessed images are stored
% in the following two directories since the scaling parameters are set to
% 3 for the main scale and 5 for the fast scale:
%
% */gdb/img/scale3
% */gdb/img/scale5
%
% The result of the framing can be visualized with the following command
% (for the key '31484183_5141'):
%
% gdb_displayFramingResult(gdbDir,'31484183_5141')
%
% To display all 32 framings use this for loop:
%
% for kk = 1:length(keysAll)
%    gdb_displayFramingResult(gdbDir,keysAll{kk})
% end
keysAll = gdb_getAllKeys(gdbDir);
for kk = 1:length(keysAll)
    disp(['Preprocessing ',keysAll{kk}]);
    gdb_preprocessImage(gdbDir,keysAll{kk});
end

% Register the first image (the seed image). The seed image in a sense
% refers to two separate images: (1) the original, un-geo-referenced image
% and (2) an image that overlaps that un-geo-referenced image that has
% already been geo-referenced. Usually this second image is created by
% manually geo-referencing the original image (e.g., in Quantum GIS). The
% association between these two images, which are stored separately in the
% database, is established above when gdb_addSeedImage is called.
gdb_registerSeedImage(gdbDir,keySeed);
gdb_writeLocalGeotiff(gdbDir,keySeed);

% Check for potential matches lexically. That is, sort the 32 images
% alphabetically and check for overlap between image 1 and image 2, then
% image 2 and image 3, etc. (31 total potential matches). This is a
% sensible first step in identifying the scene structure for many sets of
% images.

ppParam = gdb_loadPreProcParam(gdbDir);
surfParamFast = gdb_loadSurfParamFast(gdbDir);
gdb_matchImagesLexcically(gdbDir,ppParam.fastScale,surfParamFast);

% The main loop begins here. The un-registered image with the most matches
% to an already registered image is selected to register next. The geometry
% is initialized using the geometry of the previously registered image and
% matching SURF points betweens between the images. The initial estimate of
% the geometry is refined using the mutual-information based adaptive block
% registration. This is the local registration step. Once all local
% registrations are finished, a final global refinement is done (see
% below).
  
gdbStatus = gdb_getStatus(gdbDir);

while strcmp(gdbStatus.NextAction,'Register Image')
    gdb_registerImg(gdbDir,gdbStatus.keyPrev,gdbStatus.keyNext);
    gdbStatusOld = gdbStatus;
    gdbStatus = gdb_getStatus(gdbDir);
    writeTiff = true;
    if isfield(gdbStatus,'keysOutsideRef')
        if any(contains(gdbStatus.keysOutsideRef,gdbStatusOld.keyNext))
            writeTiff = false;
        end
    end
    if writeTiff
        gdb_writeLocalGeotiff(gdbDir,gdbStatusOld.keyNext);
    end
end

% Finally, refine the local fits by doing a global alignment. This is done
% iteratively in the function gdb_doGlobalFitIterative, which works much
% like the expectation maximization (EM) algorithm.

ppParam = gdb_loadPreProcParam(gdbDir);

[keysGlobFit,paramCa,etaVect] = gdb_doGlobalFitIterative(gdbDir,ppParam.mainScale);
param = paramCa{end};
gdb_saveGlobalFit(gdbDir,keysGlobFit,paramCa,etaVect);

for kk = 1:length(keysGlobFit)
    key = keysGlobFit{kk};
    gdb_writeGlobalGeotiff(gdbDir,key);
    gdb_writeGlobalGeotiffMaxRes(gdbDir,key);
end