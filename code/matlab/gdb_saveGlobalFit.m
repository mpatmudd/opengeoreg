function gdb_saveGlobalFit(gdbDir,keysGlobFit,varargin)
% Save the result of the global fit to the "geodatabase"

switch nargin
    case 3
        param = varargin{1};
        saveIterationInfo = false;
    case 4
        paramCa = varargin{1};
        etaVect = varargin{2};
        param = paramCa{end};
        saveIterationInfo = true;
    otherwise
        error('Unsupported number of inputs');
end
scaleStrMain = gdb_getScaleStrMain(gdbDir);

numKeys = length(keysGlobFit);

dirName = num2str(randi([1,10000000],1));

d = fullfile(gdbDir,'data','fit','global',scaleStrMain,dirName);
mkdir(d);

sigR = param(1);
sigI = param(2);

hFull = param(3:end);

util_saveFit(sigR,d,'sigR');
util_saveFit(sigI,d,'sigI');
    
for kk = 1:numKeys
    key = keysGlobFit{kk};
    h = hFull((1 + (kk-1)*8):(8 + (kk-1)*8));
    util_saveFit(h,d,key);
end

if saveIterationInfo
    save(fullfile(d,'iteration_info.mat'),'paramCa','etaVect');
end