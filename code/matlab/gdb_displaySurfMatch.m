function gdb_displaySurfMatch(gdbDir,key0,key1)
% For key0 and key1, display the result of SURF matching

imgName0 = [key0,'.tif'];
imgName1 = [key1,'.tif'];

scaleStrFast = gdb_getScaleStrFast(gdbDir);

surfDir = fullfile(gdbDir,'data','surf',scaleStrFast);
imgDir = fullfile(gdbDir,'img',scaleStrFast);
surfParamFast = gdb_loadSurfParamFast(gdbDir);

surfMatchData = alg_smartSurfMatch(surfParamFast,imgName0,imgName1,imgDir,surfDir);

img0 = imread(fullfile(imgDir,imgName0));
img1 = imread(fullfile(imgDir,imgName1));

showMatchedFeatures(img0,img1,surfMatchData.inlier0,surfMatchData.inlier1,'montage');
