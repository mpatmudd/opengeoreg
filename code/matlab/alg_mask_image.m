function mask = alg_mask_image(I,darkThresh,satThresh)
% Description
%   Mask the input image I by finding the largest connected region
%   after thresholding using the dark threshold (lower) and saturation
%   threshold (upper). The mask is 1 (true) for pixels in the image and
%   0 (false) for pixels outside the image.
%
% Input(s)
%   Name                Type            Dimension       Description
%   I                   uint8           [N1 x N2]       Input image to mask
%   darkThresh          numeric         [scalar]        Dark threshold (lower threshold)
%   satThresh           numeric         [scalar]        Saturation threshold (upper threshold)
% Output(s)
%   mask                boolean         [N1 x N2]       Binary mask

% This would run faster if vectorized
S = size(I);
%darkThresh=30;
bw = I > darkThresh & I < satThresh;

CC = bwconncomp(bw);

numPixels = cellfun(@numel,CC.PixelIdxList);
[biggest,idx] = max(numPixels);

mask = false(S);
mask(CC.PixelIdxList{idx}) = true;
mask = imfill(mask,'holes');