function [refTiesHash,pairTiesHash,hHash,h0Hash,G,keys]...
    = alg_extractPointCorrespondences(fitHash,imgDir,regDataHash,varargin)
%%
% Description
%   Extract point correspondences using the fit information stored in
%   fitHash and results of the adapative block registrations stored in
%   regDataHash. Two types of points are "extracted":
%
%   (1) matches from each image to the reference (the inliers of the
%       adapative block registrations)
%
%   (2) matches between overlapping images (these are recalculated using
%       the fits stored in fitData to determine which images overlap and
%       only search for SIFT matches in the overlapping portions of the
%       images
%
%   Sample call(s):
%
%   [refTiesHash,pairTiesHash,hHash,h0Hash,G,keys]...
%       = alg_extractPointCorrespondences(fitHash,imgDir,regDataHash);

% Input(s)
%   Name          Description
%   fitHash       A hash storing the fits output by the local, adaptive
%                 block registrations. The hash keys are image names.
%   imgDir        Directory of unregistered images
%   regDataHash   A hash storing the results of the block registrations
%
% Output(s)
%   refTiesHash   A hash storing data for the first type of matches (image
%                 to reference). Keys are the image names. The values are
%                 cell arrays with four entries:
%
%                 Rref -- pixel locations of matches in the reference image
%                         (homogenous coordinates)
%                 Rimg -- pixel locations of matches in the unregistered
%                         image (homogenous coordinates)
%          Rref_inlier -- inliers of Rref from adaptive block registration
%          Rimg_inlier -- inliers of Rimg from adaptive block registration
%
%   pairTiesHash  A hash storing data for the second type of matches (image
%                 to image). Keys are the image names separated by an
%                 underscore. The values are cell arrays with six entries:
%
%              inlieri -- inliers of the MSAC alg (img i)
%              inlierj -- inliers of the MSAC alg (img j)
%                 h_ij -- projective transformation taking pixels in img j
%                         to pixels in img j (8 x 1, vector representation)
%                Ch_ij -- covariance matrix for h_ij
%                 h_ji -- projective transformation taking pixels in img i
%                         to pixels in img j (8 x 1, vector representation)
%                Ch_ji -- covariance matrix for h_ji
%
%   hHash          Hash of projective transforms taking pixels in image i
%                  to pixels in the reference image, h_ri (8 x 1, vector
%                  representation). These are Hblk in fitHash. Keys are
%                  image names.
%   h0Hash         Hash of projective transforms taking pixels in image i
%                  to pixels in the reference image, h0_ri (8 x 1, vector
%                  representation), prior to adaptive block registration.
%                  Keys are image names.
%
%   G              Matrix storing number of matches between images. Only
%                  images with overlap as determined by the local, adaptive
%                  block registrations are included in G (i.e., greater
%                  than zero).
%   keys           Cell array of keys (image names)

if nargin > 3
    thresh = varargin{1};
else
    thresh = 10;
end
% Get keys (image names) from fitHash
keys = fitHash.keys;
% Determine the number of images
numImg = length(keys);
% Create an image datastore object for accessing the images
% imgDs = imageDatastore(imgDir);

% Find the proportion of overlap between images (X is a symmetric matrix
% with the proportion of overlap calculated in the reference frame).
X = alg_findPropOverlapFromFits(fitHash,imgDir);

% Iterate over images to extract correspondences with the reference image
% and identify inliers
refTiesHash = containers.Map;
hHash = containers.Map;
h0Hash = containers.Map;

for i = 1:numImg
    key = keys{i};
%     imgName = [key,'.tif'];
    regData = regDataHash(key);
    fitData = fitHash(key);
    
    % The results of the block registrations are stored in Rmov and Rfix.
    % These include outliers and are relative to the initial estimate of
    % Hri, which is H0 in fitData
    Rmov = regData{7};
    Rimg = regData{8}; % same as Rfix
    Hri0 = fitData.H0;
    Hri = fitData.Hblk;
    Tpad = regData{4}; % Tip
    Rref = alg_transform_points_homography(Hri0 * Tpad,Rmov);
    inlierMov = regData{9};
    inlierFix = regData{10};
    Rmov_inlier = [inlierMov,ones(size(inlierMov,1),1)].';
    Rimg_inlier = [inlierFix,ones(size(inlierFix,1),1)].';
    Rref_inlier = alg_transform_points_homography(Hri0 * Tpad,Rmov_inlier);
    
    refTiesHash(key) = {Rref;Rimg;Rref_inlier;Rimg_inlier};
    h0Hash(key) = alg_homogMat2Vect(Hri0);
    hHash(key) = alg_homogMat2Vect(Hri);
end

% Iterate over overlapping images to extract correspondences using SURF
% matching. Only images with overlap as stored in the matrix X are
% considered.
pairTiesHash = containers.Map;
% G is a symmetric matrix indicating whether images overlap and have enough
% matches after outlier detection (greater than minMatches = 20).
G = false(numImg,numImg);
minMatches = 20;
for i = 1:(numImg-1)
    keyi = keys{i};
    imgNamei = [keyi,'.tif'];
    imgi = imread(fullfile(imgDir,imgNamei));
    %     imgi = readimage(imgDs,i);
    %     [~,keyi,exti] = fileparts(imgDs.Files{i});
    %     imgNamei = [keyi,exti];
    fitDatai = fitHash(keyi);
    Hri = fitDatai.Hblk;
    
    for j = (i+1):numImg
        if(X(i,j) > 0)
            keyj = keys{j};
            imgNamej = [keyj,'.tif'];
            imgj = imread(fullfile(imgDir,imgNamej));
            %             imgj = readimage(imgDs,j);
            %             [~,keyj,extj] = fileparts(imgDs.Files{j});
            %             imgNamej = [keyj,extj];
            fitDataj = fitHash(keyj);
            Hrj = fitDataj.Hblk;
            
            Hij_alg = alg_invertH(Hri) * Hrj;
            Hij_alg = Hij_alg / Hij_alg(3,3);
            % By inputing H01_est in the call to alg_doSurfMatching, only
            % points in the overlap regions are considered
            [Hij_surf,numMatches,inlieri,inlierj] =...
                alg_doSurfMatching(imgi,imgj,'thresh',thresh,...
                'msacTrials',100000,'H01_est',Hij_alg,'msacRadius',10);
            disp([num2str(i),' ',num2str(j),' ',num2str(numMatches)]);
            if numMatches >= minMatches
                % If there are enough matches, populate pairTiesHash and
                % update the link matrix, G
                Rij_i = [inlieri.Location(:,2).';inlieri.Location(:,1).';ones(size(inlieri.Location(:,2).'))];
                Rji_j = [inlierj.Location(:,2).';inlierj.Location(:,1).';ones(size(inlierj.Location(:,2).'))];
                [h_ij,Ch_ij] = alg_solve_two_image_homography(Rij_i,Rji_j);
                [h_ji,Ch_ji] = alg_solve_two_image_homography(Rji_j,Rij_i);
                
                pairKey = [keyi,'_',keyj];
                pairTiesHash(pairKey) = {inlieri;inlierj;h_ij;Ch_ij;h_ji;Ch_ji};
                G(i,j) = true;
                G(j,i) = true;
            end
        end
    end
end