function img = util_getImgSmart(d,key)
% Call util_getImgFileSmart to get the path to the image for the input key
% in the directory d. Then, read in the image and return it.

f = util_getImgFileSmart(d,key);
img = imread(f);