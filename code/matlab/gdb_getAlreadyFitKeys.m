function keysFit = gdb_getAlreadyFitKeys(gdbDir)
% Return a cell array, keysFit, of images in the "geodatabase" that have
% already been fit using the adaptive block registration algorithm

scaleStrMain = gdb_getScaleStrMain(gdbDir);

keysFit = util_dir2keys(fullfile(gdbDir,'data','fit','local',scaleStrMain),...
    'extension','mat');