function M = alg_graph2matrix(G,varargin)
% Description
%   Convert an object of class graph to a corresponding symmetric edge
%   weight matrix
%
% Input(s)
%   Name              Description
%   G                 The graph object
%   defaultNaN        [optional] Flag indicating whether missing edges
%                     should be set to NaN (otherwise 0). The default is
%                     true.
%
% Output(s)
%   M                 Symmetric matrix of edge weights

% Handle the variable input (defaultNaN)
if nargin > 1
    defaultNaN = varargin{1};
else
    defaultNaN = true;
end

% Initialize the matrix
N = G.numnodes;
if defaultNaN
    M = nan(N,N);
else
    M = zeros(N,N);
end

% Loop over potential edges to populate the edge matrix
for ii = 1:(N-1)
    for jj = (ii+1):N
        kk = findedge(G,ii,jj);
        if kk > 0
            w = G.Edges{kk,'Weight'};
            M(ii,jj) = w;
            M(jj,ii) = w;
        end
    end
end