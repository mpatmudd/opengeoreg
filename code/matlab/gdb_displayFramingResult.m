function gdb_displayFramingResult(gdbDir,key)
% Display the result of the framing (done during preprocessing) for the
% input key

scaleStrMain = gdb_getScaleStrMain(gdbDir);

imgFile = fullfile(gdbDir,'img','framing',scaleStrMain,[key,'.tif']);
F = imread(imgFile);

framData = gdb_getFramingData(gdbDir,key);
vis_displayFramingResult(framData.edgeFitData,F);
