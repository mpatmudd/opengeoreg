function gdb_preprocessImage(gdbDir,key)
% Preprocess the given key in whose image is stored in the "geodatabase".
% This is a relatively complex process involving the following steps:
%
% (a) Convert RGB images to grayscale (if necessary)
% (b) Rescale the original image to the main scale (yields F, the unframed
%     image)
% (c) Do adaptive histogram equalization on the unframed image F
% (d) Attempt to frame the image (i.e., identify the active image area)
%     using a variety of settings for the dark threshold, saturation
%     threshold, and identifying the edge boundaries from either the inside
%     or outside of the image. The active area is assumed to be close to
%     rectangular, so an irregularity test is done on the shape of the
%     resulting framing for each setting. A test is also done to ensure
%     that the resulting framing is near the expected dimensions determined
%     by the input ppParam.L. Once a framing is found, the image is masked.
%     If no settings yield a suitable framing a nominal masking is done.
% (e) Redo the adaptive histrogram equalization
% (f) Rescale the image further to the fast scale
%
% The results of the framing, including the mask file, are written to the
% geodatabase so the complete chain of spatial transformations from the
% main and fast scale images to the original image can be reconstructed.
%
% The function gdb_displayFramingResult can be used to visualize the
% framing result.

imgFileIn = gdb_getOriginalImageFile(gdbDir,key);

ppParam = gdb_loadPreProcParam(gdbDir);

I = imread(imgFileIn);

if ndims(I) == 3
    I = rgb2gray(I);
end

imgInfo = imfinfo(imgFileIn);

switch imgInfo.ResolutionUnit
    case 'Inch'
        c = 1;
    case 'Centimeter'
        c = 2.54;
    otherwise
        error(['Unrecognized resolution unit: ',...
            imgInfo.ResolutionUnit]);
end

F = I;
rescaling = 1;
if ppParam.mainScale > 0
    rescaling = 2^ppParam.mainScale;
    for ii = 1:ppParam.mainScale
        F = impyramid(F,'reduce');
    end
end
F = adapthisteq(F,'clipLimit',ppParam.clipLimit,...
    'Distribution','rayleigh');

dpiu = c*imgInfo.YResolution;
dpiv = c*imgInfo.XResolution;


Lu_pix = ppParam.L * dpiu / rescaling;
Lv_pix = ppParam.L  * dpiv / rescaling;

% Try a range of framings until one works
satThresh = max(F(:)) - 5;
for ii = 0:100
    darkThresh = min(F(:)) + ii;
    
    try
        framData = alg_frame_image(F,darkThresh,satThresh,false);
        
        irreg = alg_calcFramingIrreg(framData);
        A0 = ppParam.L^2;
        A = alg_calcFramingArea(framData,dpiu,dpiv) * rescaling^2;
        rejectFraming = irreg > ppParam.irregThresh ||...
            abs( (A-A0)/A0 ) > ppParam.areaThresh;
    catch
        rejectFraming = true;
    end
    
    if ~rejectFraming
        useIn = false;
        break;
    end
    
    try
        framDataIn = alg_frame_image(F,darkThresh,satThresh,true);
        
        irreg = alg_calcFramingIrreg(framDataIn);
        A0 = ppParam.L^2;
        A = alg_calcFramingArea(framDataIn,dpiu,dpiv) * rescaling^2;
        rejectFraming = irreg > ppParam.irregThresh ||...
            abs( (A-A0)/A0 ) > ppParam.areaThresh;
    catch
        rejectFraming = true;
    end
    
    if ~rejectFraming
        useIn = true;
        break;
    end 
end

if rejectFraming
    % Then the framing may be bad. Utilize the provisional mask to
    % find the center of mask of the active image area and create
    % an edge mask using the active image dimensions (Lpix)
    framDataNew.rejected = true;
    
    provMask = alg_mask_image(F,min(F(:))+20,max(F(:))-20);
    
    [u,v] = find(provMask);
    umean = mean(u);
    vmean = mean(v);
    umin = ceil(umean - Lu_pix/2);
    umax = floor(umean + Lu_pix/2);
    vmin = ceil(vmean - Lv_pix/2);
    vmax = floor(vmean + Lv_pix/2);
    
    % If any of the corners fall outside the image, subset relative to
    % the center of the image
    if umin < 1 || vmin < 1 || umax > size(I,1) || vmax > size(I,2)
        umean = size(I,1)/2;
        vmean = size(I,2)/2;
        umin = ceil(umean - Lu_pix/2);
        umax = floor(umean + Lu_pix/2);
        vmin = ceil(vmean - Lv_pix/2);
        vmax = floor(vmean + Lv_pix/2);
    end
    framDataNew.edgeMask = false(size(I));
    framDataNew.edgeMask(umin:umax,vmin:vmax) = true;
    framDataNew.umin = umin;
    framDataNew.umax = umax;
    framDataNew.vmin = vmin;
    framDataNew.vmax = vmax;
else
    framData.rejected = false;
end

% Write the framing information to file
scaleStrMain = ['scale',num2str(ppParam.mainScale)];
scaleStrFast = ['scale',num2str(ppParam.fastScale)];

fOut = fullfile(gdbDir,'data','framing',scaleStrMain,[key,'.mat']);
if ~rejectFraming && useIn
    framData = framDataIn;
end

if rejectFraming
    framData = framDataNew;
end
save(fOut,'framData');

% Write the image used for framing to file
fOut = fullfile(gdbDir,'img','framing',scaleStrMain,[key,'.tif']);
imwrite(F,fOut);

if ~rejectFraming
    % The subsetting in the original frame
    umin = framData.edgeFitData.umin;
    umax = framData.edgeFitData.umax;
    vmin = framData.edgeFitData.vmin;
    vmax = framData.edgeFitData.vmax;
    %     end
    
    % Create the final mask and warp it back to the original frame
    x = [framData.edgeFitData.upper_left(1),...
        framData.edgeFitData.lower_left(1),...
        framData.edgeFitData.lower_right(1),...
        framData.edgeFitData.upper_right(1)];
    y = [framData.edgeFitData.upper_left(2),...
        framData.edgeFitData.lower_left(2),...
        framData.edgeFitData.lower_right(2),...
        framData.edgeFitData.upper_right(2)];
    edgeMask = poly2mask(x,y,size(F,1),size(F,2));
    
    if ppParam.applyProvMask
        finalMask = framData.provMask & edgeMask;
    else
        finalMask = edgeMask;
    end
else
    umin = framData.umin;
    umax = framData.umax;
    vmin = framData.vmin;
    vmax = framData.vmax;
    finalMask = framData.edgeMask;
end

F = F(umin:umax,vmin:vmax);

M = finalMask(umin:umax,vmin:vmax);

% Apply mask
S = size(F);
F = F(:);
M = M(:);
F(~M) = 0;
F = reshape(F,S);
M = reshape(M,S);

% Redo the histogram equalization
F = adapthisteq(F,'clipLimit',ppParam.clipLimit,...
    'Distribution','rayleigh');

% Write the main scale image and mask to file
fOut = fullfile(gdbDir,'img',scaleStrMain,[key,'.tif']);
imwrite(F,fOut);
fOut = fullfile(gdbDir,'img',scaleStrMain,['mask_',key,'.tif']);
imwrite(M,fOut);

% Write the fast scale image and mask to file
for ii = 1:ppParam.fastScale - ppParam.mainScale
    F = impyramid(F,'reduce');
    M = impyramid(M,'reduce');
end
fOut = fullfile(gdbDir,'img',scaleStrFast,[key,'.tif']);
imwrite(F,fOut);
fOut = fullfile(gdbDir,'img',scaleStrFast,['mask_',key,'.tif']);
imwrite(M,fOut);

% Write the subsetting information to file
fOut = fullfile(gdbDir,'data','subsetting',[key,'.txt']);
fid = fopen(fOut,'wt');
fprintf(fid, '%f,%f,%f,%f\n',umin,umax,vmin,vmax);
fclose(fid);