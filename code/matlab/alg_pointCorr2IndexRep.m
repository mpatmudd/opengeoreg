function [h,irRi_ca,riRr_ca,ijRi_ca,jiRj_ca,pairInfo] =...
    alg_pointCorr2IndexRep(refTiesHash,pairTiesHash,hHash,keys,G)
%%
% Description
%   Transform point correspondences (image to reference and image to image)
%   from a representation using hash maps and keys to a representation
%   using only image and pair indices.
%
%   Sample call(s):
%
%   [h,irRi_ca,riRr_ca,ijRi_ca,jiRj_ca,imgInfo,pairInfo] =...
%       alg_pointCorr2IndexRep(refTiesHash,pairTiesHash,hHash,keys,G)
    
% Input(s)
%   Name          Description
%   refTiesHash   A hash storing data for the first type of matches (image
%                 to reference). Keys are the image names. The values are
%                 cell arrays with four entries:
%                 
%                 Rref -- pixel locations of matches in the reference image
%                         (homogenous coordinates)
%                 Rimg -- pixel locations of matches in the unregistered
%                         image (homogenous coordinates)
%          Rref_inlier -- inliers of Rref from adaptive block registration
%          Rimg_inlier -- inliers of Rimg from adaptive block registration
%
%   pairTiesHash  A hash storing data for the second type of matches (image
%                 to image). Keys are the image names separated by an
%                 underscore. The values are cell arrays with six entries:
%
%              inlieri -- inliers of the MSAC alg (img i)
%              inlierj -- inliers of the MSAC alg (img j)
%                 h_ij -- projective transformation taking pixels in img j
%                         to pixels in img j (8 x 1, vector representation)
%                Ch_ij -- covariance matrix for h_ij
%                 h_ji -- projective transformation taking pixels in img i
%                         to pixels in img j (8 x 1, vector representation)
%                Ch_ji -- covariance matrix for h_ji
%   keys           Cell array of keys (image names)
%   hHash          Hash of projective transforms taking pixels in image i
%                  to pixels in the reference image, h_ri (8 x 1, vector
%                  representation). These are Hblk in fitHash. Keys are
%                  image names.
%
%   G              Matrix storing number of matches between images. Only
%                  images with overlap as determined by the local, adaptive
%                  block registrations are included in G (i.e., greater
%                  than zero).
%
% Output(s)
%h,irRi_ca,riRr_ca,ijRi_ca,jiRj_ca,imgInfo,pairInfo
%   h             Vector of projective matrices
%                   Size: 8*numImg x 1
%
%   irRi_ca       Cell array of point matches in i to the reference image r
%                   Size: numImg x 1
%
%   riRr_ca       Cell array of point matches in the reference image to i
%                   Size: numImg x 1
%
%   ijRi_ca       Cell array of point matches in i to j
%                   Size: numPairs x 1
%
%   jiRj_ca       Cell array of point matches in j to i
%                   Size: numPairs x 1
%
%   pairInfo      Matrix mapping pair index to image indices for image to
%                 image pairs. First column gives i for the pair and second
%                 column j for the pair
%                   Size: numPairs x 2

% Initialize variables
numImg = length(keys);
numPairs = length(pairTiesHash.keys);
pairInfo = zeros(numPairs,2);
irRi_ca = cell(numImg,1);
riRr_ca = cell(numImg,1);
ijRi_ca = cell(numPairs,1);
jiRj_ca = cell(numPairs,1);
h = zeros(numImg*8,1);

% Loop over images to create image to reference data
for i = 1:numImg
    key = keys{i};
    refTies = refTiesHash(key);
    irRi_ca{i} = refTies{4};
    riRr_ca{i} = refTies{3};
    h((1 + (i-1)*8):(8 + (i-1)*8)) = hHash(key);
end

% Loop over pairs to create image to image data
counter = 1;
for i = 1:(numImg-1)
    keyi = keys{i};
    for j = (i+1):numImg
        if G(i,j)
            keyj = keys{j};
            pairKey = [keyi,'_',keyj];
            pairTies = pairTiesHash(pairKey);
            inlieri = pairTies{1};
            inlierj = pairTies{2};
            ijRi_ca{counter} = [inlieri.Location(:,2).';inlieri.Location(:,1).';ones(size(inlieri.Location(:,2).'))];
            jiRj_ca{counter} = [inlierj.Location(:,2).';inlierj.Location(:,1).';ones(size(inlierj.Location(:,2).'))];
            pairInfo(counter,:) = [i,j];
            counter = counter + 1;
        end
    end
end