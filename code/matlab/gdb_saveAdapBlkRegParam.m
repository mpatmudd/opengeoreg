function gdb_saveAdapBlkRegParam(gdbDir,adapBlkRegParam)
% Save the input adapative block registration parameters to the
% "geodatabase". The paramaters are stored in the following file:
%
% */param/adaptive_block_registration_parameters.mat

f = fullfile(gdbDir,'param','adaptive_block_registration_parameters.mat');
save(f,'adapBlkRegParam');
