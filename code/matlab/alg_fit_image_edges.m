function edgeFitData = alg_fit_image_edges(bw,findEdgeFromInside)
% bw is a binary mask which is (ideally) 1 for the active, interior portion
% of the image and 0 otherwise. Fit lines to the edges of the image, where
% the edge is either found from the outside or inside.

% In Matlab, when an image is displayed the x-axis is to the right
% and the y-axis is down. However, for indexing of images stored as
% arrays the first index is for y and the second index is for x. To
% account for this, in this script y will be associated with the first
% index (i) and x with the second index (j).

% if(nargin>3)
%     doPlot = varargin{1};
% else
%     doPlot = false;
% end

% addpath('C:\Users\mhp12\bitbucket\air\preprocessing\RANSAC-Toolbox-master')
% addpath('C:\Users\mhp12\bitbucket\air\preprocessing\RANSAC-Toolbox-master\Models\Line')
% addpath('C:\Users\mhp12\bitbucket\air\preprocessing\RANSAC-Toolbox-master\Common')

S = size(bw);

edgeIgnoreProp = .1;
y = (round(S(1)*edgeIgnoreProp) : round(S(1)*(1-edgeIgnoreProp))).';
x = (round(S(2)*edgeIgnoreProp) : round(S(2)*(1-edgeIgnoreProp))).';

Nx = length(x);
Ny = length(y);

% There are four edges to the image: upper, lower, left, and right.
% The upper line corresponds to a changing x and roughly fixed y.
% The lower line corresponds to a changing x and roughly fixed y.
% The left  line corresponds to a changing y and roughly fixed x.
% The right line corresponds to a changing y and roughly fixed x.

yupper  = NaN(Nx,1); % Depends on the variable x defined above
ylower  = NaN(Nx,1); % Depends on the variable x defined above
xleft   = NaN(Ny,1); % Depends on the variable y defined above
xright  = NaN(Ny,1); % Depends on the variable y defined above

xc = round(S(2)/2);
yc = round(S(1)/2);

if(findEdgeFromInside)
    for ii = 1:Ny
        imgSlice = bw(y(ii),:);
        indx_left = find(imgSlice(:,1:xc)==0);
        indx_right = find(imgSlice(:,xc:S(2))==0);
        
        if(length(indx_left) > 1)
            xleft(ii) = max(indx_left)+1;
        else
            xleft(ii) = 1;
        end
        
        if(length(indx_right) > 1)
            xright(ii) = (xc-1) + min(indx_right)-1;
        else
            xright(ii) = size(bw,2);
        end
    end
    
    for jj = 1:Nx
        imgSlice = bw(:,x(jj));
        indy_upper = find(imgSlice(1:yc,:)==0);
        indy_lower = find(imgSlice(yc:S(1),:)==0);
        
        if(length(indy_upper) > 1)
            yupper(jj) = max(indy_upper)+1;
        else
            yupper(jj) = 1;
        end
        
        if(length(indy_lower) > 1)
            ylower(jj) = (yc-1) + min(indy_lower)-1;
        else
            ylower(jj) = size(bw,1);
        end
    end
else
    for ii = 1:Ny
        imgSlice = bw(y(ii),:);
        indx = find(imgSlice==1);
        
        if(length(indx) > 1)
            xleft(ii) = min(indx);
            xright(ii) = max(indx);
        end
    end
    
    for jj = 1:Nx
        imgSlice = bw(:,x(jj));
        indy = find(imgSlice==1);
        
        if(length(indy) > 1)
            yupper(jj) = min(indy);
            ylower(jj) = max(indy);
        end
    end
end

% disp('----there----')
% close('all');
% figure(1);
% imshow(bw);
% hold on;
% plot(x,yupper,'.r')
% plot(x,ylower,'.r')
% plot(xleft,y,'.r')
% plot(xright,y,'.r')
% pause(5)

%plot(y,xright,'color','red','lineWidth',2)
%plot(y,xleft,'color','red','lineWidth',2)
%plot(yupper,x,'color','red','lineWidth',2)
%plot(ylower,x,'color','red','lineWidth',2)

options.epsilon = 1e-6;
% options.P_inlier = 0.99;
options.P_inlier = 0.97;
options.sigma = 1;
options.est_fun = @estimate_line;
options.man_fun = @error_line;
options.mode = 'MSAC';
options.Ps = [];
options.notify_iters = [];
options.min_iters = 100;
options.fix_seed = false;
options.reestimate = true;
options.stabilize = false;
options.T_noise_squared = 1;

good = ~isnan(yupper);
Dupper = [x(good).';yupper(good).'];
[fit_upper, ~] = RANSAC(Dupper, options);
slp_upper = -fit_upper.Theta(1)/fit_upper.Theta(2);
int_upper = -fit_upper.Theta(3)/fit_upper.Theta(2);

good = ~isnan(ylower);
Dlower = [x(good).';ylower(good).'];
[fit_lower, ~] = RANSAC(Dlower, options);
slp_lower = -fit_lower.Theta(1)/fit_lower.Theta(2);
int_lower = -fit_lower.Theta(3)/fit_lower.Theta(2);

% Note the change in the first variable (x <-> y) for the following fits
good = ~isnan(xleft);
Dleft = [y(good).';xleft(good).'];
[fit_left, ~] = RANSAC(Dleft, options);
slp_left = -fit_left.Theta(1)/fit_left.Theta(2);
int_left = -fit_left.Theta(3)/fit_left.Theta(2);

good = ~isnan(xright);
Dright = [y(good).';xright(good).'];
[fit_right, ~] = RANSAC(Dright, options);
slp_right = -fit_right.Theta(1)/fit_right.Theta(2);
int_right = -fit_right.Theta(3)/fit_right.Theta(2);

% ulcx, e.g., stands for upper left corner x
[ulcx,ulcy] = alg_find_line_intersection(slp_upper,int_upper,slp_left,int_left);
[llcx,llcy] = alg_find_line_intersection(slp_lower,int_lower,slp_left,int_left);
[lrcx,lrcy] = alg_find_line_intersection(slp_lower,int_lower,slp_right,int_right);
[urcx,urcy] = alg_find_line_intersection(slp_upper,int_upper,slp_right,int_right);

upper_left  = [ulcx,ulcy];
lower_left  = [llcx,llcy];
lower_right = [lrcx,lrcy];
upper_right = [urcx,urcy];

% xmin =  ceil(max(ulcx,llcx));
% xmax = floor(min(urcx,lrcx));
% ymin =  ceil(max(ulcy,urcy));
% ymax = floor(min(llcy,lrcy));


vmin =  ceil(max(ulcx,llcx));
vmax = floor(min(urcx,lrcx));
umin =  ceil(max(ulcy,urcy));
umax = floor(min(llcy,lrcy));

%Isub = I(xmin:xmax,ymin:ymax);
% padding = 100;
% Isub = I((ymin+padding):(ymax-padding),(xmin+padding):(xmax-padding));

edgeFitData.options = options;
edgeFitData.fit_upper = fit_upper;
edgeFitData.fit_lower = fit_lower;
edgeFitData.fit_left = fit_left;
edgeFitData.fit_right = fit_right;
edgeFitData.Dupper = Dupper;
edgeFitData.Dlower = Dlower;
edgeFitData.Dleft = Dleft;
edgeFitData.Dright = Dright;

edgeFitData.upper_left = upper_left;
edgeFitData.lower_left = lower_left;
edgeFitData.lower_right = lower_right;
edgeFitData.upper_right = upper_right;

edgeFitData.umin = umin;
edgeFitData.umax = umax;
edgeFitData.vmin = vmin;
edgeFitData.vmax = vmax;
%     varargout{1} = fitData;