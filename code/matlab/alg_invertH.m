function Hinv = alg_invertH(H)
%%
% Description
%   Invert the input projective matrix.
%
% Sample call(s):
%
% Hinv = alg_invertH(H)
%
% Input(s)
%   Name          Description
%   H             Projective matrix to invert
%
% Output(s)
%   Hinv          Inverted projective matrix
Hinv = inv(H);

% Ensure that the (3,3) element equals 1
Hinv = Hinv/Hinv(3,3);
