function X = alg_findPropOverlapFromFits(fitHash,imgDir)
% Description
%   Utilize the fits in the hash map fitHash to identify which images in
%   the image directory, imgDir, overlap. Calculate the percentage overlap
%   relative to the total area spanned by both images. 0 corresponds to no
%   overlap. The keys in fitHash are the filenames of the images in imgDir.
%
% Input(s)
%   Name          Type      Description
%   fitHash       Map       Hash mapping keys (filenames) to fit info
%   imgDir        string    Directory where images are located

% Output(s)
%   X             matrix    Symmetric matrix of proportion of overlap

% Loop over keys to calculate corner polygons of all images in the
% reference frame
keys = fitHash.keys;
numImg = length(keys);

polyCw = zeros(4,2,numImg);

for i = 1:numImg
    key = keys{i};
    imgName = [key,'.tif'];
    fitData = fitHash(key);
    imgFile = fullfile(imgDir,imgName);
    
    Hri = fitData.Hblk;
    
    [u,v] = alg_getCornerPolyCw(imgFile,Hri);
    polyCw(:,:,i) = [u,v];
end

% Iterate over pairs to determine overlap
X = zeros(numImg,numImg);
for i = 1:(numImg-1)
    ui = polyCw(:,1,i);
    vi = polyCw(:,2,i);
    for j = (i+1):numImg
        uj = polyCw(:,1,j);
        vj = polyCw(:,2,j);
        [uint,vint] = polybool('intersection',ui,vi,uj,vj);
        isOverlap = ~isempty(uint);
        if isOverlap
            Ai = polyarea(ui,vi);
            Aj = polyarea(uj,vj);
            Aint = polyarea(uint,vint);        
            X(i,j) = Aint / (Ai + Aj - Aint);
            X(j,i) = X(i,j);
        end
    end
end