function M = gdb_getGlobalImgMask(gdbDir,key)
% Get the main scale mask file
f = gdb_getPathToGlobalImgMask(gdbDir,key);
M = imread(f);
M = M == 0;
