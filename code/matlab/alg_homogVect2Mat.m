function Hmat = alg_homogVect2Mat(hVect)
%%
% Description
%   Convert a projective matrix represented as a vector (eight degrees of
%   freedom) to a matrix. Ordering follows Matlab's : operator.
%
% Sample call(s):
%
% Hmat = alg_homogVect2Mat(hVect)
%
% Input(s)
%   Name          Description
%   hVect         Vector representation of projective matrix
%
% Output(s)
%   Hmat          Projective matrix to convert

Hmat = reshape([hVect;1],[3,3]);