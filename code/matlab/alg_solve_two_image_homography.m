function [H,varargout] = alg_solve_two_image_homography(Rp,R)
%%
% Description
% Solve for the homography that transforms from source/unprimed
% coordinates (R) to primed/destination coordinates(Rp). Rp and R are
% homogenous matrices of 2D points with dimensions 3 x N. See:
%
% http://www.cse.psu.edu/~rtc12/CSE486/lecture16.pdf
%
% Sample call(s):
%
% H = alg_solve_two_image_homography(Rp,R)
% [H,Ch] = alg_solve_two_image_homography(Rp,R)
%
% Input(s)
%   Name          Description
%   Rp            Points in the destination/primed frame
%   R             Points in the source/unprimed frame
%
% Output(s)
%   H             The projective matrix taking unprimed to primed points
%   Ch            [optional] An estimate of the covariance matrix of H.
%                 Ordering of elements in Ch is per the Matlab : operator
%                 (and alg_homogMat2Vect and alg_homogVect2Mat).

% Initialize variables
N = size(Rp,2);
A = nan(2*N,8);
b = nan(2*N,1);

% Iterate over points
for ii = 1:N
    x  = R(1,ii);
    y  = R(2,ii);
    xp = Rp(1,ii);
    yp = Rp(2,ii);
    A(2*(ii-1) + 1,:) = [x,y,1,0,0,0,-x*xp,-y*xp];
    A(2*(ii-1) + 2,:) = [0,0,0,x,y,1,-x*yp,-y*yp];
    b(2*(ii-1) + 1) = xp;
    b(2*(ii-1) + 2) = yp;
end

% Solve linear system
% h := [H11; H12; H13; H21; H22; H23; H31; H32]; H33 = 1
% R's stacking convention differs from the ordering in the set-up
% (and solution) of the linear system.
if nargout > 1
    mdl = fitlm(A,b,'Intercept',false);
    H = mdl.Coefficients.Estimate; % A vector, not a matrix
    indOrder = [1;4;7;2;5;8;3;6];
    H = H(indOrder);
    Ch = mdl.CoefficientCovariance;
    Ch = Ch(indOrder,indOrder);
    varargout{1} = Ch;
else
    h = A \ b;
    H = reshape([h;1],[3,3]).';
end
