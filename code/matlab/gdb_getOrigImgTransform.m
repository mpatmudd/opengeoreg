function Hrx = gdb_getOrigImgTransform(gdbDir,key)
% Return the transform that takes original image pixels (x) to reference
% image pixels (r)

% The scale reduction matrix
Hred = [.5,0,.5;0,.5,.5;0,0,1];

% i is main image
% x is original image
ppParam = gdb_loadPreProcParam(gdbDir);
framData = gdb_getFramingData(gdbDir,key);
Hix = alg_create_translation_matrix(framData.edgeFitData.umin,framData.edgeFitData.vmin) * Hred^ppParam.mainScale;
Hri = alg_homogVect2Mat(gdb_getGlobalFit(gdbDir,key));
Hrx = Hri * Hix;