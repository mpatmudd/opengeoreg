function gdb_registerImg(gdbDir,keyPrev,keyNext)
% Utilize the adapative block registration algorithm to register the next
% image (keyNext), initializing with SURF matches to the previous image
% (keyPrev).
imgNamePrev = [keyPrev,'.tif'];
imgNameNext = [keyNext,'.tif'];

surfParamMain = gdb_loadSurfParamMain(gdbDir);
scaleStrMain = gdb_getScaleStrMain(gdbDir);
imgDirMain  = fullfile(gdbDir,'img',scaleStrMain);
surfDirMain = fullfile(gdbDir,'data','surf',scaleStrMain);

surfMatchData = alg_smartSurfMatch(surfParamMain,imgNamePrev,...
    imgNameNext,imgDirMain,surfDirMain);
Hpn = alg_surfMatchData2HomogMat(surfMatchData);

[fitPrev,~] = gdb_getLocalFit(gdbDir,keyPrev);
Hrp = fitPrev.Hblk;

Hrn = Hrp * Hpn;
imgNext = imread(fullfile(imgDirMain,[keyNext,'.tif']));
adapBlkRegParam = gdb_loadAdapBlkRegParam(gdbDir);
refFile = gdb_getRefFile(gdbDir);
try
    [fitNext,regDataNext] = alg_adaptive_block_registration(imgNext,...
        refFile,Hrn,adapBlkRegParam.blockSize,...
        adapBlkRegParam.blockSpacing,adapBlkRegParam.radius,...
        adapBlkRegParam.padding);
    ppParam = gdb_loadPreProcParam(gdbDir);
    gdb_writeLocalFitToFile(gdbDir,fitNext,regDataNext,keyNext,ppParam.mainScale);  
catch e
    if ~strcmp(e.message,'ImgOutsideRef')
        error(e)
    end
    disp([keyNext,' outside reference']);
    gdb_addKeyOutsideRef(gdbDir,keyNext);
end




