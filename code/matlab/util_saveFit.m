function util_saveFit(h,d,key)
% Save the fit for the homogenous matrix h (vector representation) in the
% directory d for the given key.
f = fullfile(d,[key,'.mat']);
save(f,'h');