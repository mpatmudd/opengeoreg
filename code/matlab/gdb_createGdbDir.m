function gdb_createGdbDir(gdbDir,varargin)
% Initialize a new "geodatabase". Optional inputs are given in name/value
% pairs. These include the optional algorithm parameters ppParam,
% surfParam, surfParamFast, and adapBlkRegParam. If they are not input,
% default values are used. If the input directory, gdbDir, already exists
% it is only overwritten if the optional input replaceDir is true.

% Set defaults
replaceDir = false;
ppParam = gdb_getDefaultPrepProcParam;
surfParam = gdb_getDefaultSurfParamMain;
surfParamFast = gdb_getDefaultSurfParamFast;
adapBlkRegParam = gdb_getDefaultAdapBlkRegParam;

% If necessary, modify defaults
if nargin > 1
    % Read the pairs
    numVarArg = length(varargin);
    if ~mod(numVarArg,2) == 0
        error('Variable inputs must be in variable/value pairs');
    end
    
    for v = 1:(numVarArg/2)
        inputName = varargin{1 + (v-1)*2};
        inputVal = varargin{2 + (v-1)*2};
        
        switch lower(inputName)
            case 'replacedir'
                replaceDir = inputVal;
            case 'ppparam'
                ppParam = inputVal;
            case 'surfparammain'
                surfParam = inputVal;
            case 'surfparamfast'
                surfParamFast = inputVal;
            case 'adapblkregparam'
                adapBlkRegParam = inputVal;
            case 'createmaxresdir'
                createMaxResDir = inputVal;
            otherwise
                error('Unrecognized variable/value pair')
        end
    end
end

% Throw an error if replaceDir is false but the directory exists
if exist(gdbDir,'dir')
    if ~replaceDir
        error('Directory exists but replaceDir is false');
    else
        rmdir(gdbDir,'s');
    end
end

mkdir(gdbDir);

paramDir = fullfile(gdbDir,'param');
mkdir(paramDir);

gdb_savePreProcParam(gdbDir,ppParam);
gdb_saveSurfParamMain(gdbDir,surfParam);
gdb_saveSurfParamFast(gdbDir,surfParamFast);
gdb_saveAdapBlkRegParam(gdbDir,adapBlkRegParam);

scaleStrMain = gdb_getScaleStrMain(gdbDir);
scaleStrFast = gdb_getScaleStrFast(gdbDir);





% Add directories
dirToAdd = {...
    
fullfile(gdbDir,'img','framing',scaleStrMain);...
fullfile(gdbDir,'img','geotiffs','local',scaleStrMain);...
fullfile(gdbDir,'img','geotiffs','global',scaleStrMain);...
fullfile(gdbDir,'img','original');...
fullfile(gdbDir,'img','ref');...
fullfile(gdbDir,'img',scaleStrMain);...
fullfile(gdbDir,'img',scaleStrFast);...
fullfile(gdbDir,'img','seed');...
fullfile(gdbDir,'data','fit','global',scaleStrMain);...
fullfile(gdbDir,'data','fit','local',scaleStrMain);...
fullfile(gdbDir,'data','framing',scaleStrMain);...
%     fullfile(gdbDir,'data',scaleStrFast);...
fullfile(gdbDir,'data','subsetting');...
fullfile(gdbDir,'data','surf',scaleStrMain);...
fullfile(gdbDir,'data','surf',scaleStrFast);...
};

if createMaxResDir
    dirToAdd{length(dirToAdd)+1} = fullfile(gdbDir,'img','geotiffs','global','maxres');
end

for ii = 1:length(dirToAdd)
    d = dirToAdd{ii};
    mkdir(d)
end