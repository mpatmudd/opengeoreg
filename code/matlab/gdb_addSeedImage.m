function gdb_addSeedImage(gdbDir,key,inputFile)
% Add the input seed image to the "geodatabase". It is stored in:
%
% */img/seed
%
% The input image is a geo-referenced image that overlaps the original
% image with the input key. The file gets renamed by appending 'seed' at
% beginning and is assumed to be a .tif file.

outputFile = fullfile(gdbDir,'img','seed',[key,'.tif']);
copyfile(inputFile,outputFile);