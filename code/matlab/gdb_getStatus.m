function gdbStatus = gdb_getStatus(gdbDir)
% Get the current status of processing in the geodatabase. Use the status
% to identify the next action, which includes the following options:
%
% (1) Add Images
% (2) Add Seed Image
% (3) Add Ref Image
% (4) Preprocess Images
% (5) Register Seed Image
% (6) Register Image
% (7) Global Fit
% (8) None

keysAll = gdb_getAllKeys(gdbDir);

if length(keysAll) == 0
    gdbStatus.NextAction = 'Add Images';
    return;
end
gdbStatus.keysAll = keysAll;

keysSeed = gdb_getSeedKeys(gdbDir);
if length(keysSeed) == 0
    gdbStatus.NextAction = 'Add Seed Image';
    return;
end
gdbStatus.keysSeed = keysSeed;

keysRef = gdb_getRefKeys(gdbDir);
if length(keysRef) == 0
    gdbStatus.NextAction = 'Add Ref Image';
    return;
end
gdbStatus.keysRef = keysRef;

% No error checking is currently done to ensure that both main and fast
% preprocessed images exist
keysPp = gdb_getPreProcKeysMain(gdbDir);
if length(keysPp) > 0
    gdbStatus.keysPp = keysPp;
end

keysNotPp = setdiff(keysAll,keysPp);
if length(keysNotPp) > 0
    gdbStatus.NextAction = 'Preprocess';
    gdbStatus.keyNext = keysNotPp{1};
    return;
end

keysFit = gdb_getAlreadyFitKeys(gdbDir);
if length(keysFit) > 0
    gdbStatus.keysFit = keysFit;
end

if length(keysFit) == 0
    gdbStatus.NextAction = 'Register Seed Image';
    gdbStatus.keyNext = gdbStatus.keysSeed{1}; % If necessary, add support for more than one seed image
    return;
end

keysOutsideRef = gdb_getKeysOutsideRef(gdbDir);
if length(keysOutsideRef) > 0
    gdbStatus.keysOutsideRef = keysOutsideRef;
end

keysNotFit = setdiff(keysAll,keysFit);
keysNotFit = setdiff(keysNotFit,keysOutsideRef);
if length(keysNotFit) > 0
    gdbStatus.NextAction = 'Register Image';
    % move getting ppParam and surfParamFast inside gdb_getNextMatch
    ppParam = gdb_loadPreProcParam(gdbDir);
    surfParamFast = gdb_loadSurfParamFast(gdbDir);
    [keyPrev,keyNext] = gdb_getNextMatch(gdbDir,ppParam,surfParamFast);
    
    if isnan(keyPrev)
        error('Unable to find new match');
    else
        gdbStatus.keyPrev = keyPrev;
        gdbStatus.keyNext = keyNext;
        return;
    end
end

scaleStrMain = gdb_getScaleStrMain(gdbDir);
globFitDir = fullfile(gdbDir,'data','fit','global',scaleStrMain);

if length(dir(globFitDir)) == 2
    gdbStatus.NextAction = 'Global Fit';
else
    gdbStatus.NextAction = 'None';
end