function G = alg_buildOverlapGraph(surfDir,surfParam,allImgKeys)
% Use the stored matches in the directory surfDir to generate the overlap
% graph G. Overlap requires that the number of matching points is greater
% than or equal to surfParam.minMatch. The edge weights of the graph G are
% the number of matches.

files = dir( [surfDir,'/match*.mat']);
files = { files.name}; % get only file names

kp = KeyPattern('',AttributePattern('match','.mat'));
matchKeys = cellfun(@(s) kp.extractKey(s),files,'UniformOutput',false);

edgeInd0 = [];
edgeInd1 = [];
edgeWeight = [];

for m = 1:length(matchKeys)
    tokens = strsplit(matchKeys{m},'_');
    numTok = length(tokens);
    key0 = strjoin(tokens(1:(numTok/2)),'_');
    key1 = strjoin(tokens((1+numTok/2):end),'_');
    imgName0 = [key0,'.tif'];
    imgName1 = [key1,'.tif'];
    
    
    k0 = find(strcmp(key0,allImgKeys));
    k1 = find(strcmp(key1,allImgKeys));
    if length(k0) > 0 & length(k1) > 0
        surfMatchData = alg_smartSurfMatch(surfParam,imgName0,imgName1,'',surfDir);
        
        edgeInd0 = [edgeInd0 k0];
        edgeInd1 = [edgeInd1 k1];
        
        if surfMatchData.numMatches >= surfParam.minMatch
            edgeWeight = [edgeWeight surfMatchData.numMatches];
        else
            edgeWeight = [edgeWeight 0];
        end
    end
end

G = graph(edgeInd0,edgeInd1,edgeWeight,allImgKeys);