function adapBlkRegParam = gdb_loadAdapBlkRegParam(gdbDir)
% Load the adaptive block registration parameters from the relevant file in
% the "geodatabase"
f = fullfile(gdbDir,'param','adaptive_block_registration_parameters.mat');
adapBlkRegParam = load(f,'adapBlkRegParam');
adapBlkRegParam = adapBlkRegParam.adapBlkRegParam;
