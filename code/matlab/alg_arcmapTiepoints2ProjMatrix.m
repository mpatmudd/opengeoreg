function Hri = alg_arcmapTiepoints2ProjMatrix(tieFile,dpi,imgFile,refFile)
% Description
%   Calclulate the projective matrix taking image pixels (i/e) to reference
%   pixels using tie points identifie din ArcMAP. The reference image
%   coordinates of the tie points are stored in geo-coordinates and the
%   image coordinates are stored using ArcMAP's assumed coordinate system
%   (which includes an inherent pixel spacing, dpi).
%
% Sample call(s):
%
% Hri = alg_arcmapTiepoints2ProjMatrix(tieFile,dpi,imgFile,refFile)
% 
% Input(s)
%   Name          Description
%   tieFile       Tie-points file
%   dpi           Pixel spacing detected or assumed by ArcMAP in original
%                 image. This is not stored with the geo-referencing points
%                 saved by ArcMAP to an external text file. dpi stands for
%                 dots per inch, but this could be in another distance
%                 unit, too (e.g., meters or feet).
%   imgFile       The unregistered image file (i/e)
%   refFile       The reference image file (r)
%
% Output(s)
%   Hri           The projective matrix taking image pixels (i/e) to
%                 reference pixels (r)

% Read in the data
img = imread(imgFile);
regData = dlmread(tieFile);
% Calculate coordinates using Matlab pixel and ordering conventions
[uimg,vimg] = alg_arcImgCoordToMatlabPixels(regData(:,1),regData(:,2),size(img,1),dpi);
[uref,vref] = alg_arcGeoCoordToMatlabPixels(regData(:,3),regData(:,4),refFile);

% Represent coordinates using homogenous coordinates
Rimg = [uimg.';vimg.';ones(size(uimg.'))];
Rref = [uref.';vref.';ones(size(uref.'))];

% Solve for the projective transformation (or homography)
Hri = alg_solve_two_image_homography(Rref,Rimg);
