function keys = util_dir2keys(d,varargin)
%%
% Description
%   Identify keys from the filenames in the directory d. There are three
%   optional inputs read in as named-value pairs that control the behavior of
%   the function: before, after, and extension. before requires the
%   filename to start with the input string (ignoring extension), after
%   requires the filename to end with the input string (ignoring extension),
%   and extension requires the filename to have the given extension. If
%   present, the before, after, and extension strings are removed from the
%   key.
%
% Input(s)
%   Name          Description
%   d             Directory of files corresponding to the keys
%   before        [optional] String that begins the key (ignoring extension)
%   after         [optional] String that ends the key (ignoring extension)
%   extension     [optional] Extension (only files with this extension are
%                            valid keys)
%
% Output(s)
%   fkeys         Cell array of keys

useBefore = false;
useAfter = false;
useExtension = false;
if nargin > 2
    % Read the pairs
    numVarArg = length(varargin);
    if ~mod(numVarArg,2) == 0
        error('Variable inputs must be in variable/value pairs');
    end
    
    for v = 1:(numVarArg/2)
        inputName = varargin{1 + (v-1)*2};
        inputVal = varargin{2 + (v-1)*2};
        
        switch lower(inputName)
            case 'before'
                before = inputVal;
                useBefore = true;
                if isa(before,'char')
                    before = {before};
                end
                
            case 'after'
                after = inputVal;
                useAfter = true;
                if isa(after,'char')
                    after = {after};
                end
                
            case 'extension'
                extensions = inputVal;
                useExtension = true;
                if isa(extensions,'char')
                    extensions = {extensions};
                end
                
            otherwise
                error('Unrecognized variable/value pair')
        end
        
    end
end

% Get a cell array of files in the directory
files = dir(d);
files = files(~[files.isdir]); % Removes '.' and '..' and other directories
files = {files.name}; % Creates a cell array of filenames

keep = true(size(files));
keys = cell(1,0);
% Handle the file extension
for ii = 1:length(files)
    f = files{ii};
    [~,name,ext] = fileparts(f);
    ext = ext(2:end); % Remove the period
    if useExtension
        if any(cellfun(@(s) endsWith(ext,s),extensions))
            keys = [keys,name];
        end
    else
        % All file extensions are valid
        keys = [keys,name];
    end
end

% Handle before [if necessary]
if useBefore
    keep = false(size(keys));
    for ii = 1:length(keys)
        s = keys{ii};
        for jj = 1:length(before)
            if startsWith(s,before{jj})
                keys{ii} = s((length(before{jj})+1):end);
                keep(ii) = true;
                break;
            end
        end
    end
    keys = keys(keep);
end

% Handle after [if necessary]
if useAfter
    keep = false(size(keys));
    for ii = 1:length(keys)
        s = keys{ii};
        for jj = 1:length(after)
            if endsWith(s,after{jj})
                keys{ii} = s(1:(length(s)-length(after{jj})));
                keep(ii) = true;
                break;
            end
        end
    end
    keys = keys(keep);
end

keys = unique(keys);
