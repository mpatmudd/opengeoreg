function gdb_matchImagesLexcically(gdbDir,scale,surfParam)
% Often, images in the geodatabase that overlap each other are near each
% other when their keys are lexically sorted (for example, because they are
% sequential images in a run). This function sorts the keys from the
% "geodatabase" lexically and checks for overlap in lexically adjacent
% pairs. If N is the number of keys, N-1 pairs are checked.

scaleStr = ['scale',num2str(scale)];

imgDir = fullfile(gdbDir,'img',scaleStr);
keys = sort(util_dir2keys(imgDir,'extension','tif'));
keysMask = util_filterStart(keys,'mask_');

keys = setdiff(keys,keysMask);

keys0 = keys(1:(end-1));
keys1 = keys(2:end);
gdb_matchImages(gdbDir,scale,surfParam,keys0,keys1)
