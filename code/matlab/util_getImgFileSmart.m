function imgFilename = util_getImgFileSmart(d,key)
% Get the path to the image file for the input key in directory d. If there
% is a .tif file, the path to it is returned. If there is not but there is
% a .txt file, read the path to the image in the .txt file. If neither a
% .tif or .txt file exists, throw an error.

fTif = fullfile(d,[key,'.tif']);
if exist(fTif,'file')
    imgFilename = fTif;
    return;
end

fTxt = fullfile(d,[key,'.txt']);
if exist(fTxt,'file')
    fid = fopen(fTxt,'r');
    imgFilename = fgetl(fid);
    fclose(fid);
    return;
else
    error('Could not find image matching this key');
end