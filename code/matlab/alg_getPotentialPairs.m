function pairInfo = alg_getPotentialPairs(G,indFit)
% Get a list of potential pairs from the overlap graph, G, given the
% indices that have already been fit, indFit.

neighb = alg_findAllNeighbNodes(G,indFit);

pairs = zeros(0,2);
weights = zeros(0,1);

for ii = 1:length(neighb)
    prev = intersect(alg_findAllNeighbNodes(G,neighb(ii)),indFit);
    if length(prev) > 0
        pairs = [pairs;[prev,repmat(neighb(ii),length(prev),1)]];
        for jj = 1:length(prev)
            idxOut = findedge(G,prev(jj),neighb(ii));
            weights = [weights; G.Edges.Weight(idxOut)];
        end
    end
end

pairInfo = [pairs,weights];