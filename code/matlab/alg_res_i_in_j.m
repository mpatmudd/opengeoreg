function resj = alg_res_i_in_j(Hji,ui,vi)
% Calculate the resolution of spatial reference i in j at the point ui/vi.

R0i = [ui;vi;1];
R1i = [ui;vi;1] + [1;1;0];
R0j = alg_transform_points_homography(Hji,R0i);
R1j = alg_transform_points_homography(Hji,R1i);

dUj = abs(R1j(1) - R0j(1));
dVj = abs(R1j(2) - R0j(2));

resj = mean([dUj,dVj]);