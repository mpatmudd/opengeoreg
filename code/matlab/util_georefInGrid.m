function util_georefInGrid(imgDir,key,dataDir,refFile,Nu,Nv,rotation,varargin)
% Georeference an image with an overlayed grid to ensure distribution of
% tie-points

if ~any(rotation == [0,90,180,270])
    error('Rotation must be 0, 90, 180, or 270 degrees');
end

if nargin > 7
    lineWidth = varargin{1};
else
    lineWidth = 1;
end

if nargin > 8
    % No error checking is done to ensure that a save image was created
    % with the same inputs
    useSaveImg = true;
    saveImgFile = varargin{2};
    haveSaveImg = exist(saveImgFile,'file');
else
    useSaveImg = false;
end

if useSaveImg && haveSaveImg
    img = imread(saveImgFile);
else
    imgFile = fullfile(imgDir,[key,'.tif']);
    img = imread(imgFile);
    
    if rotation == 90
        img = rot90(img);
    end
    
    if rotation == 180
        img = rot90(img,2);
    end
    
    if rotation == 270
        img = rot90(img,3);
    end
    
    img = adapthisteq(img,'clipLimit',.008,'Distribution','rayleigh');
    img = cat(3,img,img,img);
    
    
    
    u = linspace(1,size(img,1),Nu+1);
    u = round(u(2:(end-1)));
    
    v = linspace(1,size(img,2),Nv+1);
    v = round(v(2:(end-1)));
    
    w = (1 - floor(lineWidth / 2)) : ceil(lineWidth/2);
    
    % B = zeros([size(img,1),size(img,2),3]);
    
    for ii = 1:length(u)
        for kk = 1:length(w)
            img(u(ii) + w(kk),1:size(img,2),1) = 200;
        end
    end
    
    for jj = 1:length(v)
        for kk = 1:length(w)
            img(1:size(img,1),v(jj) + w(kk),1) = 200;
        end
    end
end

if useSaveImg && ~haveSaveImg
    imwrite(img,saveImgFile);
end

dataFile = fullfile(dataDir,[key,'.mat']);
ref = imread(refFile);

if exist(dataFile,'file')
    pointData = load(dataFile);
    movPoints = pointData.movPoints;
    fixPoints = pointData.fixPoints;
    if ~isfield(pointData,'rotation')
        pointData.rotation = 0; % For backwards compatability with files created before rotation was an input
    end
    
    if rotation ~= pointData.rotation
        error('Input rotation does not agree with saved rotation');
    end
    
    [movPoints,fixPoints] = cpselect(img,ref,movPoints,fixPoints,'Wait',true);
else
    [movPoints,fixPoints] = cpselect(img,ref,'Wait',true);
end
save(dataFile,'movPoints','fixPoints','rotation');
