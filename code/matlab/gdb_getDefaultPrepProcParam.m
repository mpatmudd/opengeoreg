function ppParam = gdb_getDefaultPrepProcParam
% Description
%   Return default pre-processing parameters
% Input(s)
%   Name              Description
%   [none]
%
% Output(s)
%   ppParam           Structure containing the following fields:
%     mainScale       Rescaling factor for main processing steps
%     fastScale       Rescaling factor for fast processing [speed traded
%                     off against accuracy]
%     L               Dimension of active image area [area = L^2 = 81 in^2]
%     irregThresh     Threshold for accepting the irregularity [see
%                     gdb_preprocessImage]
%     areaThresh      Tolerance on the difference from the nominal active
%                     image area [L^2; see gdb_preprocessImage]
%     clipLimit       Clip limit for adaptive histogram equalization
%     applyProvMask   Boolean flag indicating whether to apply the
%                     provisional mask to the overall mask [see
%                     gdb_preprocessImage]

ppParam.mainScale = 3;
ppParam.fastScale = 5;
ppParam.L = 9;
ppParam.irregThresh = 0.004;
ppParam.areaThresh = 0.004;
ppParam.clipLimit = 0.008;
ppParam.applyProvMask = true;