function adapBlkRegParam = gdb_getDefaultAdapBlkRegParam
% Description
%   Return default adaptive block registration parameters
% Input(s)
%   Name              Description
%   [none]
%
% Output(s)
%   adapBlkRegParam   Structure containing the following fields:
%     padding         Padding to use in creation of padded reference image
%                     (relative to the unregistered image)
%     blockSize       Block size for block registrations
%     blockSpacing    Block spacing for block registrations
%     radius          Radius (in pixels) for block outlier detection


% Padding to use in creation of padded reference image (relative to the
% unregistered image)
adapBlkRegParam.padding = .1;
% Block size for block registrations
adapBlkRegParam.blockSize = 200;
% Block spacing for block registrations
adapBlkRegParam.blockSpacing = 100;
% Radius (in pixels) for block outlier detection
adapBlkRegParam.radius = 2;