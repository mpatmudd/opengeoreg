function keys = gdb_getSeedKeys(gdbDir)
% Get the keys for the seed images from the "geodatabase". The images or
% paths to them are stored in */img/seed
seedImgDir = fullfile(gdbDir,'img','seed');
keys = util_dir2keys(seedImgDir,'extension',{'tif';'txt'});