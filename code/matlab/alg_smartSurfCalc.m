function surfPointData = alg_smartSurfCalc(surfParam,varargin)

% img
% mat
%
% imgName imgDir
% imgName imgDs
% imgName imgDir surfDir
% imgName imgDs  surfDir
% imgName imgDir surfHash
% imgName imgDs  surfHash

% Patterns of variable inputs
% ----------------------
% (1)
% matrix
% img
% ----------------------
% (2)
% str     str
% imgName imgDir
% ----------------------
% (3)
% str     datastore
% imgName imgDs
% ----------------------
% (4)
% str     str         str
% imgName imgDir      surfDir
% ----------------------
% (5)
% str     datastore   str
% imgName imgDs       surfDir
% ----------------------
% (6)
% str     str         hash
% imgName imgDir      surfHash
% ----------------------
% (7)
% str     datastore   hash
% imgName imgDs       surfHash

% Handle variable inputs
if ~(nargin==2 || nargin==3 || nargin==4)
    error('Unsupported number of inputs');
end

useRepo = nargin == 4;

if nargin == 2
    img = varargin{1};
    haveImg = true;
else
    % nargin = 3 or 4
    haveImg = false;
    if ischar(varargin{1}) && ischar(varargin{2})
        % ----------------------
        % alg_smartSurfCalc(surfParam,imgName,imgDir,...)
        % [~,string,string,...]
        %             imgDir = varargin{2};
        %             imgPath = fullfile(imgName,imgDir);
        imgDir = varargin{2};
        useImgDir = true;
    elseif ischar(varargin{1}) &&...
            isa(varargin{2},'matlab.io.datastore.ImageDatastore')
        % ----------------------
        % alg_smartSurfCalc(surfParam,imgName,imgDs,...)
        % [~,string,datastore,...]
        imgDs = varargin{2};
        useImgDir = false;
    else
        error('Usupported input types');
    end
    imgName = varargin{1};
    tokens = strsplit(imgName,'.');
    key = tokens{1}; % does not include file extension
end

if nargin == 4
    if ischar(varargin{3})
        % ----------------------
        % alg_smartSurfCalc(surfParam,~,~,surfDir)
        % [~,~,~,string]
        useSurfDir = true;
        surfDir = varargin{3};
    elseif isa(varargin{3},'containers.Map')
        % ----------------------
        % alg_smartSurfCalc(surfParam,~,~,surfHash)
        % [~,~,~,hash]
        useSurfDir = false;
        surfHash = varargin{3};
    else
        error('Usupported input types');
        
    end
    % alg_smartSurfCalc(surfParam,imgName,imgDs,...)
    % [~,string,datastore,...]
end

if useRepo
    if useSurfDir
        % Look for the key in the directory
        % The format of the filename is
        % surf_points_key.mat
        surfFilename = ['point',key,'.mat'];
        files = dir( [surfDir,'/*.mat']);
        files = { files.name}; % get only file names
        ind = find(contains(files,surfFilename));
        if ~isempty(ind)
            varStruct = load(fullfile(surfDir,surfFilename));
            surfPointData = varStruct.v;
            return;
        end
    else
        % Look for the key in the hash
        keys = surfHash.keys;
        ind = find(contains(keys,key));
        if ~isempty(ind)
            surfPointData = surfHash(key);
            return;
        end
    end
end

% If this point is reached, the data must be calculated
if ~haveImg
    if useImgDir
        img = imread(fullfile(imgDir,imgName));
    else
        allImgNames = util_extractFilename(imgDs);
        imgInd = find(contains(allImgNames,imgName));
        img = readimage(imgDs,imgInd);
    end
end

if ndims(img) > 2
    img = rgb2gray(img);
end

points = detectSURFFeatures(img,'MetricThreshold',surfParam.thresh);

if any(contains(fieldnames(surfParam),'numPoints'))
    points = points.selectStrongest(surfParam.numPoints);
end


[features,validPoints] = extractFeatures(img,points);
surfPointData.points = points;
surfPointData.features = features;
surfPointData.validPoints = validPoints;
surfPointData.surfParam = surfParam;

if useRepo
    if useSurfDir
        v = surfPointData;
        save(fullfile(surfDir,surfFilename),'v');
    else
        surfHash(key) = surfPointData;
    end
end



