function surfParamFast = gdb_loadSurfParamFast(gdbDir)
% Load the surf calculation and matching parameters (fast scale) from file
% in the "geodatabase". They are stored in:
%
% */param/surf_param_fast.mat

f = fullfile(gdbDir,'param','surf_param_fast.mat');
surfParamFast = load(f,'surfParamFast');
surfParamFast = surfParamFast.surfParamFast;