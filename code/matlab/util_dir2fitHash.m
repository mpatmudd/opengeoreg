function [fitHash,varargout] = util_dir2fitHash(d)
%%
% Description
%   d is a directory that contains Matlab save files (.mat extension).
%   Create a hash map (containers.Map object) in which the keys are the
%   filenames (without the .mat extension) and the values are the fit data
%   in the save files (i.e., the variable fit in the saved data).
%
% Input(s)
%   Name          Description
%   gd            The directory containing the fit data
%
% Output(s)
%   fitHash       The hash map for fit data
%   regDataHash   [optional] A hash map containing registration information

outputRegDataHash =  nargout > 1;

fitHash = containers.Map;
if outputRegDataHash
    regDataHash = containers.Map;
end

keys = util_dir2keys(d,'extension','mat');

for kk = 1:length(keys)
    key = keys{kk};
    f = fullfile(d,[key,'.mat']);
    savedData = load(f); % Could save a small amount of time by only reading in regData if necessary
    fitHash(key) = savedData.fit;
    if outputRegDataHash
        regDataHash(key) = savedData.regData;
    end
end

if outputRegDataHash
    varargout{1} = regDataHash;
end