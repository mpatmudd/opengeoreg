function surfMatchData = alg_revSurfMatchData(surfMatchData)
% Reverse the input surfMatchData. This is necessary because the surf
% matches have an ordering when they are stored, but sometimes the images
% are retrieved in the opposite ordering.

if surfMatchData.numMatches == 0
    return
end

% Swap matchedPoints0 and matchedPoints1
matchedPoints0_old = surfMatchData.matchedPoints0;
surfMatchData.matchedPoints0 = surfMatchData.matchedPoints1;
surfMatchData.matchedPoints1 = matchedPoints0_old;

% Swap inlier0 and inlier1
inlier0_old = surfMatchData.inlier0;
surfMatchData.inlier0 = surfMatchData.inlier1;
surfMatchData.inlier1 = inlier0_old;

% Reverse columns on indexPairs
surfMatchData.indexPairs = [surfMatchData.indexPairs(:,2),...
    surfMatchData.indexPairs(:,1)];

% Invert tform
surfMatchData.tform = invert(surfMatchData.tform);