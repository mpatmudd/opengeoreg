function [paramFull,irRr_ca,ijRr_ca,jiRr_ca,distSqrR_vect,distSqrI_vect] =...
    alg_updateIterativeNllSoln(paramFull,hri,pairInfo,i,irRi_ca,riRr_ca,ijRi_ca,jiRj_ca,irRr_ca,ijRr_ca,jiRr_ca,distSqrR_vect,distSqrI_vect,Nr,Ni)
% Update the solution for the iterative reprojection problem

% Update hri in the parameter vector
paramFull((3 + (i-1)*8):(10 + (i-1)*8)) = hri;
Hri = alg_homogVect2Mat(hri);

% Update image to reference match
irRr_ca{i} = alg_transform_points_homography(Hri,irRi_ca{i});
X = irRr_ca{i}(1:2,:) - riRr_ca{i}(1:2,:);
distSqrR_vect(i) = sum(X(:).^2);

% Update the image to image matches
ind = (pairInfo(:,1) == i) | (pairInfo(:,2) == i);
ind = find(ind);
for k = 1:length(ind)
    p = ind(k);
    rev = pairInfo(p,2) == i;
    if ~rev
        ijRr_ca{p} = alg_transform_points_homography(Hri,ijRi_ca{p});
    else
        jiRr_ca{p} = alg_transform_points_homography(Hri,jiRj_ca{p}); % j = i
    end
    X = ijRr_ca{p}(1:2,:) - jiRr_ca{p}(1:2,:);
    distSqrI_vect(p) = sum(X(:).^2);
end

% Update the standard deviations
% [A little efficiency could be gained by not re-doing the entire sums for
%  the  distSqrR_vect as distSqrR_other and distSqrI_other are already
%  calculated.]
distSqrR = sum(distSqrR_vect);
paramFull(1) = sqrt(distSqrR/2/Nr);

distSqrI = sum(distSqrI_vect);
paramFull(2) = sqrt(distSqrI/2/Ni);