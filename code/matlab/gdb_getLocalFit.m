function [fit,regData] = gdb_getLocalFit(gdbDir,key)
% Read the local fit (from the adaptive block registration) from the
% "geodatabase" for the given key. The corresponding registration data is
% also returned (regData)

scaleStrMain = gdb_getScaleStrMain(gdbDir);

fitFile = fullfile(gdbDir,'data','fit','local',scaleStrMain,[key,'.mat']);
fitObj = load(fitFile);
fit = fitObj.fit;
regData = fitObj.regData;