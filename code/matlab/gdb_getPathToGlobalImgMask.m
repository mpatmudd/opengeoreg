function f = gdb_getPathToGlobalImgMask(gdbDir,key)
% Get the main scale mask file
scaleStrMain = gdb_getScaleStrMain(gdbDir);

f = fullfile(gdbDir,'img','geotiffs','global',scaleStrMain,['mask_',key,'.tif']);