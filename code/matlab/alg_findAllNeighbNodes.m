function neighb = alg_findAllNeighbNodes(G,nodes,varargin)
% Description
%   Find all neighbors of the nodes in the eponymous variable "nodes" given
%   the graph G. By default, all nodes in "nodes" are excluded from the
%   vector of neighbors.
%
% Sample call(s):
%
% neighb = alg_findAllNeighbNodes(G,nodes,)
% neighb = alg_findAllNeighbNodes(G,nodes,excludeExisting)
%
% Input(s)
%   Name            Description
%   G               A graph object indicating which nodes are connected
%   nodes           A vector of nodes for which the neighbors are needed
%   excludeExisting A boolean variable indicating whether to exclude nodes
%                   "nodes" from the list of neighbors
%
% Output(s)
%   neighb          The vector of neighbors

if nargin > 2
    excludeExisting = varargin{1};
else
    excludeExisting = true;
end

% (1) Add neighbors
neighb = [];
for ii = 1:length(nodes)
    neighb = [neighb;G.neighbors(nodes(ii))];
end

% Remove neighbors already in nodes, if necessary
if excludeExisting
    neighb = setdiff(neighb,nodes);
end
