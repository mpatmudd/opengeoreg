function [irD2,ijD2] =...
    alg_calcDistSqrInRef(h,irRi_ca,riRr_ca,ijRi_ca,jiRj_ca,pairInfo)
%%
% Description
%   Project point matches to the reference frame and calculate the squared
%   distance between them. This includes both image to reference and image
%   to image correspondences (see alg_extractPointCorrespondences). Points
%   are stored in homogenous coordinates in the cell arrays
%   irRi_ca / riRr_ca [size {numImg x 1}] and ijRi_ca / jiRj_ca [size
%   {numPairs x 1}]. The notation xyRz indicates that the points are for
%   the images x and y and are the matches in image x (yx indicates the
%   corresponding matches in image y). The z to the right of R indicates
%   that the points are projected in frame z.
%
%   All matrices are indexed by integers (instead of, say, image names or
%   hash map keys). The matrix pairInfo [size [numPairs x 1]] gives the
%   index of the first image (i; first column) and second image (j; second
%   column).
%
%   Sample call(s):
%
%   [irD2,ijD2] =...
%       alg_calcDistSqrInRef(h,irRi_ca,riRr_ca,ijRi_ca,jiRj_ca,pairInfo)
    
% Input(s)
%   Name          Description
%   h             Vector of projective matrices
%                   Size: 8*numImg x 1
%
%   irRi_ca       Cell array of point matches in i to the reference image r
%                   Size: numImg x 1
%
%   riRr_ca       Cell array of point matches in the reference image to i
%                   Size: numImg x 1
%
%   ijRi_ca       Cell array of point matches in i to j
%                   Size: numPairs x 1
%
%   jiRj_ca       Cell array of point matches in j to i
%                   Size: numPairs x 1
%
%   pairInfo      Matrix mapping pair index to image indices for image to
%                 image pairs. First column gives i for the pair and second
%                 column j for the pair
%                   Size: numPairs x 2
%
% Output(s)
%   irD2          Matrix of squared distances of image to reference
%                 correspondences
%   ijD2          Matrix of squared distances of image to image
%                 correspondences

% Determine the number of images and number of image to image pairs
numImg = length(irRi_ca);
numPairs = length(ijRi_ca);

% Loop over images to calculate distance squared. Store results in a cell
% array (this allows parallel processing to be used -- parfor instead of
% for)
irD2 = cell(1,numImg);
parfor i = 1:numImg
    Hri = alg_homogVect2Mat(h((1 + (i-1)*8):(8 + (i-1)*8)));
    irRr = alg_transform_points_homography(Hri,irRi_ca{i});
    X = irRr(1:2,:) - riRr_ca{i}(1:2,:);
    irD2{i} = sum(X.^2);
end
irD2 = cell2mat(irD2); % Convert cell array to matrix

% Loop over image to image pairs to calculate distance squared. Store
% results in a cell array (this allows parallel processing to be used --
% parfor instead of for)
ijD2 = cell(1,numPairs);
parfor p = 1:numPairs
    i = pairInfo(p,1);
    j = pairInfo(p,2);
    Hri = alg_homogVect2Mat(h((1 + (i-1)*8):(8 + (i-1)*8)));
    Hrj = alg_homogVect2Mat(h((1 + (j-1)*8):(8 + (j-1)*8)));
    ijRr = alg_transform_points_homography(Hri,ijRi_ca{p});
    jiRr = alg_transform_points_homography(Hrj,jiRj_ca{p});
    X = ijRr(1:2,:) - jiRr(1:2,:);
    ijD2{p} = sum(X.^2);
end
ijD2 = cell2mat(ijD2); % Convert cell array to matrix