% Description
%   Define a pattern for a key used in key mapping. This is easiest to
%   illustrate with examples.
%
%   Example 1 -- Key with one field and no additional patterning
%     key      pattern
%     5170     5170
%     5138     5138
%     ...      ...
%
%   Example 1 -- Key with one field and no additional patterning
%     keyPatt = KeyPattern(sep);
%     ----------------
%     key               pattern
%     5170              5170
%     5138              5138
%     ...               ...
%
%   Example 2 -- Key with two fields and no additional patterning
%     keyPatt = KeyPattern('_',[true,true]);
%     ----------------
%     key               pattern
%     31484140_5170     31484140_5170
%     31484140_5138     31484140_5138
%     31484139_5170     31484139_5170
%     ...               ...
%   Example 3 -- Key with two fields and attribute patterning on the second
%                key
%     attrPatt2 = AttributePattern('img','');
%     keyPatt = KeyPattern('_',{true,attrPatt2});
%     ----------------
%     key               pattern
%     31484140_5170     31484140_img5170
%     31484140_5138     31484140_img5138
%     31484139_5170     31484139_img5170
%     ...      ...
%   Example 4 -- Key with two fields, attribute patterning on the second
%                key-field, and an ignored first attribute
%     attrPatt3 = AttributePattern('img','');
%     keyPatt = KeyPattern('_',{false,true,attrPatt3});
%     ----------------
%     key               pattern
%     31484140_5170     dataFromMike_31484140_img5170
%     31484140_5138     dataFromAnne_31484140_img5138
%     31484139_5170     dataFromMike_31484139_img5170
%     ...      ...
classdef KeyPattern
    properties
        sep;
        fieldMask=true;
        attrPattCa = cell(0);
        ignoredFieldCa = cell(0);
    end
    methods
        function obj = KeyPattern(sep,varargin)
            obj.sep = sep;
            if iscell(varargin{1})
                patterns = varargin{1};
                N = length(patterns);
                fieldMask = false(1,N);
                attrPattCa = cell(0);
                ignoredFieldCa = cell(0);
                counter = 1;
                counterIgn = 1;
                for n = 1:N
                    if isa(patterns{n},'AttributePattern')
                        fieldMask(n) = true;
                        attrPattCa{counter} = patterns{n};
                        counter = counter + 1;
                    elseif isa(patterns{n},'logical')
                        if patterns{n}
                            fieldMask(n) = true;
                            attrPattCa{counter} = AttributePattern('','');
                            counter = counter + 1;
                        else
                            ignoredFieldCa{counterIgn} = '';
                            counterIgn = counterIgn + 1;
                        end
                    elseif isa(patterns{n},'char')
                        ignoredFieldCa{counterIgn} = patterns{n};
                        counterIgn = counterIgn + 1;
                    else
                        error('Unsupported class for pattern input');
                    end
                    
                end
            else
                fieldMask = true;
                attrPattCa = {varargin{1}};
                ignoredFieldCa = cell(0);
            end
            obj.fieldMask = fieldMask;
            obj.attrPattCa = attrPattCa;
            obj.ignoredFieldCa = ignoredFieldCa;
        end
        
        function key = extractKey(obj,fullStr)
            % Add validity check
            tokens = strsplit(fullStr,obj.sep);
            if length(tokens) ~= length(obj.fieldMask)
                error(strcat('Too many tokens split by separator [',obj.sep,']'));
            end
            tokens = tokens(obj.fieldMask);
            
            N = length(tokens);
            subKeys = cell(1,N);
            for n = 1:N
                subKeys{n} = obj.attrPattCa{n}.extractValue(tokens{n});
            end
            key = strjoin(subKeys,obj.sep);
        end
        
        function B = isValid(obj,fullStr)
            % Add validity check
            tokens = strsplit(fullStr,obj.sep);
            if length(tokens) ~= length(obj.fieldMask)
                B = false;
                return;
            end
            
            N = length(tokens);
            for n = 1:N
                attrPatt = obj.attrPattCa{n};
                if ~attrPatt.isValid(fullStr)
                    B = false;
                    return;
                end
            end            
            B = true;
        end
        
        function pattern = createPattern(obj,key)
            tokens = strsplit(key,obj.sep);
            if length(tokens) ~= sum(obj.fieldMask)
                error('Incorrect key size');
            end
            N = length(tokens);
            subPatt = cell(1,N);
            for n = 1:N
                subPatt{n} = obj.attrPattCa{n}.createPattern(tokens{n});
            end
            
            pattern = cell(1,length(obj.fieldMask));
            pattern(obj.fieldMask) = subPatt;
            pattern(~obj.fieldMask) = obj.ignoredFieldCa;
            pattern = strjoin(pattern,obj.sep);    
        end
        
    end
end