function G = gdb_getOverlapGraph(gdbDir,varargin)
% Read the stored overlap graph from file. If it has yet to be calculated
% or the optional second input, calcGraph, is true then first calculate the
% graph by calling gdb_calcOverlapGraph

if nargin > 1
    calcGraph = varargin{1};
else
    calcGraph = false;
end

outFile = fullfile(gdbDir,'data','G.mat');
if ~exist(outFile,'file')
    calcGraph = true;
end

if calcGraph
    gdb_calcOverlapGraph(gdbDir);
end
outFile = fullfile(gdbDir,'data','G.mat');
G = load(outFile,'G');
G = G.G;