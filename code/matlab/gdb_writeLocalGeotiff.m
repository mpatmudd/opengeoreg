function gdb_writeLocalGeotiff(gdbDir,key)
% Use the local fit to write a geotiff to file for the input key.

scaleStrMain = gdb_getScaleStrMain(gdbDir);
localFit = gdb_getLocalFit(gdbDir,key);
imgFile = fullfile(gdbDir,'img',scaleStrMain,[key,'.tif']);
refFile = gdb_getRefFile(gdbDir);

outImgFile = fullfile(gdbDir,'img','geotiffs','local',scaleStrMain,[key,'.tif']);
outMaskFile = fullfile(gdbDir,'img','geotiffs','local',scaleStrMain,['mask_',key,'.tif']);
img = imread(imgFile);
util_write_geotiff_using_homography(outImgFile,img,localFit.Hblk,refFile,'outMaskFile',outMaskFile);