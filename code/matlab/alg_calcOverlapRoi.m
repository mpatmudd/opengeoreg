function [roi0,varargout] = alg_calcOverlapRoi(img0,img1,H01)
% Description
%   Find the intersection between img0 and img1 based on the projective
%   transform matrix H01. Calculate a rectangular region of interest
%   containing this overlap in both img0 and img1 for a call to, e.g.,
%   detectSURFFeatures.
%
% Input(s)
%   Name          Type      Description
%   img0          image     The first image (or file containing image)
%   img1          image     The second image (or file containing image)
%   H01           matrix    [3 x 3] projective matrix taking pixels in frame
%                           1 to pixels in frame 0
%
% Output(s)
%   roi0          vector    The region of interest as a vector for img0
%   roi1          vector    The region of interest as a vector for img1


% Transform the corners of img1 into the img0 pixel space and store as a
% polygon with counter-clockwise vertex ordering
[u1_in_0,v1_in_0] = alg_getCornerPolyCw(img1,H01);
% Create the polygon for img0 for it's own corners
[u0_in_0,v0_in_0] = alg_getCornerPolyCw(img0,eye(3));
% Find the intersection
[uint,vint] = polybool('intersection',u0_in_0,v0_in_0,u1_in_0,v1_in_0);

% Deal with pixels outside img0
vmin = max(1,min(vint));
umin = max(1,min(uint));
umax = min(size(img0,1),max(uint));
vmax = min(size(img0,2),max(vint));

% Create the region of interest for img0
roi0 = [vmin,umin,vmax-vmin+1,umax-umin+1];

% If necessary, repeat the process in the opposite direction
if nargout > 1
    roi1 = alg_calcOverlapRoi(img1,img0,alg_invertH(H01));
    varargout{1} = roi1;
end