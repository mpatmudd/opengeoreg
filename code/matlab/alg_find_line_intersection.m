function [x,y] = alg_find_line_intersection(a1,b1,a2,b2)
% a1/b1 define a line in which x is the independent variable
% a2/b2 define a line in which y is the independent variable
%
% The task is to solve the following system of equations for x and y:
%
% y = a1*x + b1
% x = a2*y + b2
%
% The solution is:
%
% x = (a2*b1 + b2) / (1 - a1*a2)
% y = (a1*b2 + b1) / (1 - a1*a2)

x = (a2*b1 + b2) / (1 - a1*a2);
y = (a1*b2 + b1) / (1 - a1*a2);