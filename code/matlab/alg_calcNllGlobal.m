function eta =...
    alg_calcNllGlobal(param,irRi_ca,riRr_ca,ijRi_ca,jiRj_ca,pairInfo)
% Description
%   Calculate the negative log-likelihood, eta, given the input parameter
%   vector param = [sigR;sigI;h] and point-correspondence information.
%
% The full negative log likelihood for one set of points with random error
% sig is:
%
% N*log(2*pi) + 2*N*log(sig) + SquaredError/2/sig^2
%
% The first term can be ignored since it does not change.
%
%   Sample call(s):
%
%   eta =...
%       alg_calcNllGlobal(param,irRi_ca,riRr_ca,ijRi_ca,jiRj_ca,pairInfo)
    
% Input(s)
%   Name          Description
%   param         Vector of parameters to be optimized consisting of the
%                 standard deviation for image to reference correspondences
%                 (sigR), standard deviation for image to image
%                 correspondences (sigI), and vector of projective
%                 transformations, h. param = [sigR;sigI;h].
%
%   irRi_ca       Cell array of point matches in i to the reference image r
%                   Size: numImg x 1
%
%   riRr_ca       Cell array of point matches in the reference image to i
%                   Size: numImg x 1
%
%   ijRi_ca       Cell array of point matches in i to j
%                   Size: numPairs x 1
%
%   jiRj_ca       Cell array of point matches in j to i
%                   Size: numPairs x 1
%
%   pairInfo      Matrix mapping pair index to image indices for image to
%                 image pairs. Only the first to columns, which stored the
%                 i and j values, are used.
%                   Size: numPairs x 4
%
% Output(s)
%   eta           The negative log-likelihood

% Extract parameters
sigr = param(1);
sigi = param(2);
h = param(3:end);

% Calculate distance squared for (1) image to reference correspondences and
% (2) image to image correspondences
[irD2,ijD2] = alg_calcDistSqrInRef(h,irRi_ca,riRr_ca,ijRi_ca,jiRj_ca,pairInfo);

% Calculate eta, the negative log-likelihood
Nr = length(irD2);
Ni = length(ijD2);
eta = 2*Nr*log(sigr) + sum(irD2)/2/sigr^2 +...
    2*Ni*log(sigi) + sum(ijD2)/2/sigi^2;