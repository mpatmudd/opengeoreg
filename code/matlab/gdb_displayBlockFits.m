function gdb_displayBlockFits(gdbDir,key,varargin)
% Display the results of the adaptive block registration algorithm for the
% input key. The optional input goodOnly (default false) controls whether
% all alignments are shown or only those that were accepted by the outlier
% detection.

if nargin > 2
    goodOnly = varargin{1};
else
    goodOnly = false;
end

%     regData = {affine_ca,affine0_ca,Tsr,Tep,padLim,refSubLim,...
%         Rmov,Rfix,inlierMov,inlierFix,Rref};

scaleStrMain = gdb_getScaleStrMain(gdbDir);
refFile = gdb_getRefFile(gdbDir);
[fit,regData] = gdb_getLocalFit(gdbDir,key);

imgFile = fullfile(gdbDir,'img',scaleStrMain,[key,'.tif']);
img = imread(imgFile);

adapBlkRegParam = gdb_loadAdapBlkRegParam(gdbDir);

[refSubInImg,~,~,~,~] =...
    alg_placeRefInImg(refFile,img,fit.H0,adapBlkRegParam.padding);

padLim = regData{5};

refSubInImg = refSubInImg((2 - padLim(1)):(2 - padLim(1)+size(img,1)-1),(2 - padLim(3)):(2 - padLim(3)+size(img,2)-1));

Tfm = alg_invertH(alg_create_translation_matrix(padLim(1),padLim(3)));

imshowpair(refSubInImg,img);
hold on;
if goodOnly
    inlierMov = regData{9};
    inlierFix = regData{10};
    Rmov_good = [inlierMov,ones(size(inlierMov,1),1)].';
    Rfix_good = [inlierFix,ones(size(inlierFix,1),1)].';
    Rmov_in_fix_good = alg_transform_points_homography(Tfm,Rmov_good);
    plot(Rfix_good(2,:),Rfix_good(1,:),'y.','MarkerSize',12);
    plot(Rmov_in_fix_good(2,:),Rmov_in_fix_good(1,:),'r.','MarkerSize',12);
else
    Rmov = regData{7};
    Rfix = regData{8};
    Rmov_in_fix = alg_transform_points_homography(Tfm,Rmov);
    plot(Rfix(2,:),Rfix(1,:),'y.','MarkerSize',12);
    plot(Rmov_in_fix(2,:),Rmov_in_fix(1,:),'r.','MarkerSize',12);
end