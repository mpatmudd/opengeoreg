function v = util_filterStart(v,s,varargin)
% Filter the strings in the cell array v so that only strings that start
% with s are kept. The optional input removeEnd indicates whether the
% output cell array should include the starting string, s (default is no
% [false]).

if nargin > 2
    removeStart = varargin{1};
else
    removeStart = false;
end
keep = cellfun(@(x) startsWith(x,s),v);

v = v(keep);

if removeStart
    v = cellfun(@(x) x((length(s)+1):end),v,'UniformOutput',false);
end
