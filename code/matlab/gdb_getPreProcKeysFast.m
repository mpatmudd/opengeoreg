function keys = gdb_getPreProcKeysFast(gdbDir)
% Get the keys of images that have been preprocessed by identifying .tif
% files in the following directory (fast scale):
%
% */img/scaleStrFast

scaleStrFast = gdb_getScaleStrFast(gdbDir);


keys = util_dir2keys(fullfile(gdbDir,'img',scaleStrFast),...
    'extension','tif');

keys = setdiff(keys,util_filterStart(keys,'mask_'));