function v = util_filterEnd(v,s,varargin)
% Filter the strings in the cell array v so that only strings that end with
% s are kept. The optional input removeEnd indicates whether the output
% cell array should include the ending string, s (default is no [false]).

if nargin > 2
    removeEnd = varargin{1};
else
    removeEnd = false;
end
keep = cellfun(@(x) endsWith(x,s),v);

v = v(keep);

if removeEnd
    v = cellfun(@(x) x(1:(end-length(s))),v,'UniformOutput',false);
end
