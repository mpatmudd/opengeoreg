function gdb_registerSeedImage(gdbDir,keySeed)
% Use the adapative block registration algorithm to register the seed image
% (keySeed). It is initialized using an already geo-referenced image.

refFile = gdb_getRefFile(gdbDir);
            
scaleStrMain = gdb_getScaleStrMain(gdbDir);
% imgName = [keySeed,'.tif'];
dirSeed = fullfile(gdbDir,'img','seed');
dirNew  = fullfile(gdbDir,'img',scaleStrMain);
imgFileSeed = util_getImgFileSmart(dirSeed,keySeed);
imgSeed = imread(imgFileSeed);
if ndims(imgSeed) == 3
    imgSeed = rgb2gray(imgSeed);
end
imgNew  = util_getImgSmart(dirNew, keySeed);

surfParamMain = gdb_loadSurfParamMain(gdbDir);
surfMatchData = alg_smartSurfMatch(surfParamMain,imgSeed,imgNew);

% s is seed image pixels
% n is new image pixels
% r is reference image pixels
% w is world coordinates [assumed same for seed and reference images]
Hsn = alg_surfMatchData2HomogMat(surfMatchData);
Hws = util_geotiff2Hwp(imgFileSeed);
Hwr = util_geotiff2Hwp(refFile);
Hrn = alg_invertH(Hwr) * Hws * Hsn;

adapBlkRegParam = gdb_loadAdapBlkRegParam(gdbDir);

[fitNew,regDataNew] = alg_adaptive_block_registration(imgNew,refFile,Hrn,...
    adapBlkRegParam.blockSize,adapBlkRegParam.blockSpacing,...
    adapBlkRegParam.radius,adapBlkRegParam.padding);

ppParam = gdb_loadPreProcParam(gdbDir);
gdb_writeLocalFitToFile(gdbDir,fitNew,regDataNew,keySeed,ppParam.mainScale);