function scaleStrFast = gdb_getScaleStrFast(gdbDir)
% Helper function to generate the string for fast processing:
%
% If the main scale is 5 the result is 'scale5'

ppParam = gdb_loadPreProcParam(gdbDir);
scaleStrFast = ['scale',num2str(ppParam.fastScale)];