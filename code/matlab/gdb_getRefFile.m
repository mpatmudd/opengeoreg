function refFile =gdb_getRefFile(gdbDir)
% Get the path to the reference image
%
% If necessary, support for more than one reference image could be added
keysRef = gdb_getRefKeys(gdbDir);
refFile = util_getImgFileSmart(fullfile(gdbDir,'img','ref'),keysRef{1});