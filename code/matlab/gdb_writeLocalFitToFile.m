function gdb_writeLocalFitToFile(gdbDir,fit,regData,key,scale)
% Write the local fit (i.e., the output of the adapative block
% registration) to the "geodatabase".

scaleStr = ['scale',num2str(scale)];

fitDirLocal = fullfile(gdbDir,'data','fit','local',scaleStr);
fitFile = fullfile(fitDirLocal,[key,'.mat']);

save(fitFile,'fit','regData');