function [N,inlierHash] = alg_findOverlappingImagesExhaustive(imgDs,thresh,radius,varargin)
% Description
%   Iteratet over pairs of images in the image datastore imgDs and use SURF
%   keypoint matching to determine if the images overlap.
%
% Sample call(s):
%
% [N,inlierHash] = alg_findOverlappingImagesExhaustive(imgDs,thresh,radius)
% [N,inlierHash] = alg_findOverlappingImagesExhaustive(imgDs,thresh,radius,...
%                                                  numPoints)
%
% Input(s)
%   Name          Description
%   imgDs         Image datastore of images to check
%   thresh        Metric Threshold for SURF feature detection
%   radius        Radius (Max Distance) for rejection of outliers
%   numPoints     [optional] Number of points to use in matching
% Output(s)
%   N             Vector storing the number of matching keypoints after
%                 outlier detection. N is indexed by pair indices, which
%                 are derived from the image indices in the image datastore
%                 using lexicographical combinadic ordering. This is the
%                 same convention as used by the Matlab function squareform
%                 and further details are available at:
%         [https://msdn.microsoft.com/en-us/library/aa289166(v=vs.71).aspx]
%   inlierHash    Hash map of inliers after outlier detection. The keys are
%                 pair indices converted to strings (since Matlab does not
%                 allow integers to be keys in hash maps).

% Handle variable inputs
if nargin > 3
    numPoints = varargin{1};
else
    numPoints = -1;
end
% Iterate over all pairs to detect overlap.
p = 0;
numImg = imgDs.numpartitions;
numPairs = nchoosek(numImg,2);
N = zeros(numPairs,1);

t = 0;
blocks = 0;
blockSize = 10000;
inlierHash = containers.Map;
for m0 = 1:(numImg-1)
    img0 = readimage(imgDs,m0);
    pts0 = detectSURFFeatures(img0,'MetricThreshold',thresh);
    
    if numPoints > 0
        pts0 = pts0.selectStrongest(numPoints);
    end
    
    [feat0,validPoints0] = extractFeatures(img0,pts0);
    
    for m1 = (m0+1):numImg
        p = p + 1;
        if mod(p,blockSize) == 1
            tic;
        end
        
        if mod(p,blockSize) == 0
            dt = toc;
            t = t + dt;
            blocks = blocks + 1;
            disp('----------------------------------------');
            disp(['Block of ',num2str(blockSize),' finished in ',num2str(dt)]);
            disp([num2str(blockSize*blocks),' pairs in ', num2str(t)]);
            timeLeft = (numPairs-p) * t / (blockSize*blocks);
            disp(['Estimated time left: ',num2str(timeLeft)]);
        end
        
        img1 = readimage(imgDs,m1);
        pts1 = detectSURFFeatures(img1,'MetricThreshold',thresh);
        
        if numPoints > 0
            pts1 = pts1.selectStrongest(numPoints);
        end
        
        [feat1,validPoints1] = extractFeatures(img1,pts1);
        
        
        indexPairs = matchFeatures(feat0,feat1,'method','Approximate');
        matchedPoints0 = validPoints0(indexPairs(:,1));
        matchedPoints1 = validPoints1(indexPairs(:,2));
        
        try
            [tform, inlier0, inlier1] = estimateGeometricTransform(matchedPoints0, matchedPoints1, 'projective','MaxDistance',radius,'MaxNumTrials',10000);
            numMatches = size(inlier0,1);
            inlierHash(num2str(p)) = {inlier0,inlier1};
        catch exception
            numMatches = 0;
        end
        disp([p,numMatches])
        N(p) = numMatches;
        close all;
    end
end