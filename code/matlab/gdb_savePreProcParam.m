function gdb_savePreProcParam(gdbDir,ppParam)
% Save the input preprocessing parameters to the "geodatabase". The
% paramaters are stored in the following file:
%
% */param/preprocessing_parameters.mat

ppFile = fullfile(gdbDir,'param','preprocessing_parameters.mat');
save(ppFile,'ppParam');
