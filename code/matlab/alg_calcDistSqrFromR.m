function [distSqrR_vect,distSqrI_vect,Nr,Ni] = alg_calcDistSqrFromR(irRr_ca,riRr_ca,ijRr_ca,jiRr_ca)
% Calculate the squared distance from the input homogenous matrices for the
% pairs irRr_ca/riRr_ca and ijRr_ca/jiRr_ca. See alg_calcRInRef for an
% explanation of the notation for these homogenous matrices.

numRef = length(irRr_ca);
distSqrR_vect = nan(numRef,1);
Nr_vect = nan(numRef,1);
parfor i = 1:numRef
    X = irRr_ca{i}(1:2,:) - riRr_ca{i}(1:2,:);
    distSqrR_vect(i) = sum(X(:).^2);
    Nr_vect(i) = size(X,2);
end

numPairs = length(ijRr_ca);
distSqrI_vect = nan(numPairs,1);
Ni_vect = nan(numPairs,1);
parfor p = 1:numPairs
    X = ijRr_ca{p}(1:2,:) - jiRr_ca{p}(1:2,:);
    distSqrI_vect(p) = sum(X(:).^2);
    Ni_vect(p) = size(X,2);
end

Nr = sum(Nr_vect);
Ni = sum(Ni_vect);