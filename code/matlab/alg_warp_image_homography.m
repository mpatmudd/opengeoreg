function mov_img_in_fix = alg_warp_image_homography(Hfm,varargin)
% Description
%   Warp the moving image to be in the fixed image frame.
%
%   Sample call(s):
%   mov_img_in_fix  = alg_warp_image_homography(Hfm,fixImg,movImg);
%   mov_img_in_fix  = alg_warp_image_homography(Hfm,ufix,vfix,movImg);
%
% Input(s)
%   Name        Type                Description
%   Hfm         numeric [3 x 3]     The projective matrix that transforms 
%                                   moving pixel coordinates to fixed pixel
%                                   coordinates.
%   fixImg      unit8 [F1 x F2]     The fixed image
%   ufix        int   [K x 1]       Vector of fixed coordinates (dim. 1)
%   vfix        int   [K x 1]       Vector of fixed coordinates (dim. 2)
%   movImg      uint8 [M1 x M2]     The moving image
%
% Output(s)
%   mov_img_in_fix
%   ...         unit8 [F1 x F2]     The moving image warped to be in the
%                                   fixed image coordinates.

if nargin == 4
    ufix = varargin{1};
    vfix = varargin{2};
    movImg = varargin{3};
else
    Sfix = size(varargin{1});
    ufix = (1:Sfix(1)).';
    vfix = (1:Sfix(2)).';
    movImg = varargin{2};
end
Smov = size(movImg);

% Transform the fixed image coordinates to moving image coordinates for
% interpolation
[Ufix,Vfix] = meshgrid(ufix,vfix);
Wfix = [Ufix(:),Vfix(:),ones(length(Ufix(:)),1)].';
Hmf = alg_invertH(Hfm);
Wmov = alg_transform_points_homography(Hmf,Wfix);
Umov = reshape(Wmov(1,:),size(Ufix));
Vmov = reshape(Wmov(2,:),size(Ufix));

% Interpolate the moving image pixel values at the transformed points
[Umov0,Vmov0] = meshgrid((1:Smov(1)).',(1:Smov(2)).');
if ndims(movImg) == 2
    mov_img_in_fix = uint8(interp2(Umov0,Vmov0,double(movImg.'),Umov,Vmov)).';
else
    Nbb = size(movImg,3);
    mov_img_in_fix = repmat(uint8(0),[size(Umov,2),size(Umov,1),Nbb]);
    for bb = 1:Nbb
        mov_img_in_fix(:,:,bb) = uint8(interp2(Umov0,Vmov0,double(movImg(:,:,bb).'),Umov,Vmov)).';
    end
end