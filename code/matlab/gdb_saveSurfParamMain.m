function gdb_saveSurfParamMain(gdbDir,surfParamMain)
% Save the input surf calculation and matching parameters (main scale) to
% the "geodatabase". The paramaters are stored in the following file:
%
% */param/surf_param_main.mat

f = fullfile(gdbDir,'param','surf_param_main.mat');
save(f,'surfParamMain');
