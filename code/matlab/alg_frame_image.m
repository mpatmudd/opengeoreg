function framData = alg_frame_image(I,darkThresh,satThresh,findEdgeFromInside)
% Description
%   alg_frame_image performs three tasks. First, it calls alg_mask_image to
%   create an initial, provisional mask of the image. Then, it calls
%   alg_fit_image_edges to fit four lines (using RANSAC) to the image edges
%   in order to identify the image coordintes. Third and finally, it
%   updates the provisional mask by excluding all points outside the region
%   defined by the five edge lines.
%
%   The coordinate system for the edge fit has an origin at [1,1] (not
%   [0,0]!) in the upper left corner, with the x-axis running right and
%   the y-axis running down. upper_left,lower_left,lower_right,upper_right
%   are all defined with respect to this coordinate system.
%
% Input(s)
%   Name                Type            Dimension       Description
%   I                   uint8           [N1 x N2]       Input image to mask
%   darkThresh          numeric         [scalar]        Dark threshold (lower threshold)
%   satThresh           numeric         [scalar]        Saturation threshold (upper threshold)
%   findEdgeFromInside  boolean         [scalar]        Boolean variable specifying whether alg_fit_image_edges
%                                                       should find the image edges from the inside or outside
%   irregThresh         numeric         [scalar]        Threshold for rejecting irregular fits
%   edgeSize            integer         [scalar]        Size of edges (used for irregular fits)
% Output(s)
%   upper_left          numeric         [1 x 2]         Upper left corner from edge fit
%   lower_left          numeric         [1 x 2]         Lower left corner from edge fit
%   lower_right         numeric         [1 x 2]         Lower right corner from edge fit
%   upper_right         numeric         [1 x 2]         Upper right corner from edge fit
%   provMask            boolean         [N1 x N2]       Provisional binary mask (output of alg_mask_image)
%   edgeMask            boolean         [N1 x N2]       Mask from edge fits
%   finalMask           boolean         [N1 x N2]       Provisional binary mask (accounting for identified edges)

provMask = alg_mask_image(I,darkThresh,satThresh);

edgeFitData = alg_fit_image_edges(provMask,findEdgeFromInside);

x = [edgeFitData.upper_left(1),edgeFitData.lower_left(1),...
    edgeFitData.lower_right(1),edgeFitData.upper_right(1)];
y = [edgeFitData.upper_left(2),edgeFitData.lower_left(2),...
    edgeFitData.lower_right(2),edgeFitData.upper_right(2)];

edgeMask = poly2mask(x,y,size(I,1),size(I,2));


framData.edgeFitData = edgeFitData;
framData.provMask = provMask;
framData.edgeMask = edgeMask;
framData.imgSize = size(I);
