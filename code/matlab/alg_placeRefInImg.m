function [refSubInImg,Tsr,Tep,padLim,refSubLim] =...
    alg_placeRefInImg(refFile,img,Hre,padding)
%%
% Description
% Transform the reference image to the image "plane". This involves a
% number of operations and, strictly, the transformation is to a padded
% version of the img so that the image is contained within the final output
% image (refSubInImg). The steps are:
%
% (a) Find the padded coordinates of the output image in image coordinates
% (b) Transform those points to reference image coordinates
% (c) Read a block of the reference image that contains these transformed
%     points. This image is called refSub. [Step (2) and (3) are necessary
%     because the reference image can be very large.]
% (d) Warp refSub to the padded image coordinates, appropriately accounting
%     for all translations along the way.
%
%   Sample call(s):
%
% [refSubInImg,Tref,Tpad,padLim,refSubLim] =...
% alg_placeRefInImg(refFile,img,Hre,padding)
%
% Input(s)
%   Name          Description
%   refFile       The reference image file (frame r)
%   img           The unregistered image (frame i/e)
%   Hre           The projective matrix that takes errored image pixels (e)
%                 to reference pixels (i.e., the initial estimate of the
%                 transformation)
%   padding       The padding to use relative to the size of img in
%                 creating the transformed reference image
%
% Output(s)
%   refSubInImg   The reference image transformed into the unregistered
%                 image's pixel space (with padding)
%   Tsr           The translation taking reference image pixels to
%                 sub-setted reference image pixels
%   Tep           The translation taking padded pixels (i.e., refSubInImg
%                 pixels to errored image pixels
%   padLim        The corner coordinates of the padded image (p) in the
%                 image frame (i/e)
%   refSubLim     The corner coordinates of the sub-setted reference image
%                 (s) in the reference image frame (r)


%% (a) Find the padded coordinates of the output image in image coordinates
umin = round(1 - size(img,1)*padding);
umax = round(size(img,1) + size(img,1)*padding);
vmin = round(1 - size(img,2)*padding);
vmax = round(size(img,2) + size(img,2)*padding);

%% (b) Transform those points to reference image coordinates
padCoordInImg = [umin,umin,umax,umax;vmin,vmax,vmin,vmax;1,1,1,1];
padCoordInRef = alg_transform_points_homography(Hre,padCoordInImg);

% N is the pixel padding for reading the referencing image
N = 1000;
xmin = round(min(padCoordInRef(1,:)) - N);
xmax = round(max(padCoordInRef(1,:)) + N);
ymin = round(min(padCoordInRef(2,:)) - N);
ymax = round(max(padCoordInRef(2,:)) + N);

%% (c) Read a block of the reference image that contains these transformed
%      points.
refInfo = geotiffinfo(refFile);
if xmin < 1 | xmax > refInfo.Height | ymin < 1 | ymax > refInfo.Width
    error('ImgOutsideRef','Image is outside reference image');
end
refSub = imread(refFile,'PixelRegion',{[xmin,xmax],[ymin,ymax]});

% Tsr takes reference pixels (r) to sub-setted reference pixels (s)
% Hse takes errored image pixels (e) to sub-setted reference pixels (s)
Tsr = alg_create_translation_matrix(xmin,ymin);
Hse = Tsr * Hre;

%% (d) Warp refSub to the padded image coordinates, appropriately
%      accounting for all translations along the way.
refSubInImg = alg_warp_image_homography(alg_invertH(Hse),umin:umax,vmin:vmax,refSub);
if ndims(refSubInImg) == 3
    refSubInImg = rgb2gray(refSubInImg);
end
padLim = [umin,umax,vmin,vmax];
refSubLim = [xmin,xmax,ymin,ymax];
Tep = alg_invertH(alg_create_translation_matrix(umin,vmin));