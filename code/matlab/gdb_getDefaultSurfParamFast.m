function surfParamFast = gdb_getDefaultSurfParamFast
% Description
%   Return default parameters for the SURF keypoint identification and
%   matching [fast scale]
% Input(s)
%   Name              Description
%   [none]
%
% Output(s)
%   surfParamFast     Structure containing the following fields:
%     thresh          Metric threshold for call to detectSURFFeatures
%     radius          Max distance for call to estimateGeometricTransform
%     maxTrials       Maximum number of trials in call to
%                     estimateGeometricTransform
%     minMatch        Minimum number of matching points required to acccept
%                     that two images overlap

surfParamFast.thresh = 1;
surfParamFast.radius = 2;
surfParamFast.minMatch = 15;
surfParamFast.maxTrials = 10000;