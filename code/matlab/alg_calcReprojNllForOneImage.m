function eta =...
    alg_calcReprojNllForOneImage(h,sigR,sigI,Nr,Ni,distSqrR_other,distSqrI_other,Rr,Ri,numRefPoints)
% Calculate the negative log-likelihood for the reprojection error for a
% single image with projective matrix h (vector representation).
Hri = alg_homogVect2Mat(h);
Ri_in_r = alg_transform_points_homography(Hri,Ri);
Xref = Rr(1:2,1:numRefPoints) - Ri_in_r(1:2,1:numRefPoints);
Ximg = Rr(1:2,(numRefPoints+1):end) - Ri_in_r(1:2,(numRefPoints+1):end);

distSqrR = distSqrR_other + sum(Xref(:).^2);
distSqrI = distSqrI_other + sum(Ximg(:).^2);

% Calculate eta, the negative log-likelihood
eta = 2*Nr*log(sigR) + distSqrR/2/sigR^2 +...
    2*Ni*log(sigI) + distSqrI/2/sigI^2;