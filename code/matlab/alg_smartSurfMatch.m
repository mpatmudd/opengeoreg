function surfMatchData = alg_smartSurfMatch(surfParam,varargin)

% img0 img1
% mat0 mat1
%
% imgName0 imgName1 imgDir
% imgName0 imgName1 imgDs
% imgName0 imgName1 imgDir surfDir
% imgName0 imgName1 imgDs  surfDir
% imgName0 imgName1 imgDir surfHash
% imgName0 imgName1 imgDs  surfHash

% Patterns of variable inputs
% ----------------------
% (1)
% matrix matrix
% img0   img1
% ----------------------
% (2)
% str     str      str
% imgName imgName1 imgDir
% ----------------------
% (3)
% str      str      datastore
% imgName0 imgName1 imgDs
% ----------------------
% (4)
% str      str      str         str
% imgName0 imgName1 imgDir      surfDir
% ----------------------
% (5)
% str      str      datastore   str
% imgName0 imgName1 imgDs       surfDir
% ----------------------
% (6)
% str      str      str         hash
% imgName0 imgName1 imgDir      surfHash
% ----------------------
% (7)
% str      str      datastore   hash
% imgName0 imgName1 imgDs       surfHash

% Handle variable inputs
if ~(nargin==3 || nargin==4 || nargin==5)
    error('Unsupported number of inputs');
end

useRepo = nargin == 5;

if nargin == 3
    img0 = varargin{1};
    img1 = varargin{2};
    haveImg = true;
else
    % nargin = 4 or 5
    haveImg = false;
    if ischar(varargin{1}) && ischar(varargin{2}) && ischar(varargin{3})
        % ----------------------
        % alg_smartSurfCalc(surfParam,imgName0,imgName1,imgDir,...)
        % [~,string,string,string,...]
        imgDir = varargin{3};
        useImgDir = true;
    elseif ischar(varargin{1}) && ischar(varargin{2})...
            && isa(varargin{3},'matlab.io.datastore.ImageDatastore')
        % ----------------------
        % alg_smartSurfCalc(surfParam,imgName0,imgName1,imgDs,...)
        % [~,string,string,datastore,...]
        imgDs = varargin{3};
        useImgDir = false;
    else
        error('Usupported input types');
    end
    imgName0 = varargin{1};
    imgName1 = varargin{2};
    tokens0 = strsplit(imgName0,'.');
    tokens1 = strsplit(imgName1,'.');
    key0 = tokens0{1}; % does not include file extension
    key1 = tokens1{1}; % does not include file extension
end

if nargin == 5
    if ischar(varargin{4})
        % ----------------------
        % alg_smartSurfCalc(surfParam,~,~,~,surfDir)
        % [~,~,~,~,string]
        useSurfDir = true;
        surfDir = varargin{4};
    elseif isa(varargin{4},'containers.Map')
        % ----------------------
        % alg_smartSurfCalc(surfParam,~,~,~,surfHash)
        % [~,~,~,~,hash]
        useSurfDir = false;
        surfHash = varargin{4};
    else
        error('Usupported input types');
    end
end

if useRepo
        rev = string(key1) < string(key0);
    if rev
        key = [key1,'_',key0];
    else
        key = [key0,'_',key1];
    end
    if useSurfDir
        % Look for the key in the directory
        % The format of the filename is
        % surf_matches_key.mat
        
        surfFilename = ['match',key,'.mat'];
        files = dir( [surfDir,'/*.mat']);
        files = { files.name}; % get only file names
        ind = find(contains(files,surfFilename));
        if ~isempty(ind)
            varStruct = load(fullfile(surfDir,surfFilename));
            surfMatchData = varStruct.v;
            if rev
                surfMatchData = alg_revSurfMatchData(surfMatchData);
            end
            return;
        end
    else
        % Look for the key in the hash
        keys = surfHash.keys;
        ind = find(contains(keys,key));
        if ~isempty(ind)
            surfMatchData = surfHash(key);
            if rev
                surfMatchData = alg_revSurfMatchData(surfMatchData);
            end
            return;
        end
    end
end

% (1)
% matrix
% img
% ----------------------
% (2)
% str     str
% imgName imgDir
% ----------------------
% (3)
% str     datastore
% imgName imgDs
% ----------------------
% (4)
% str     str         str
% imgName imgDir      surfDir
% ----------------------
% (5)
% str     datastore   str
% imgName imgDs       surfDir
% ----------------------
% (6)
% str     str         hash
% imgName imgDir      surfHash
% ----------------------
% (7)
% str     datastore   hash
% imgName imgDs       surfHash


% If this point is reached, the data must be calculated
if ~haveImg
    if useImgDir
        if useRepo
            if useSurfDir
                surfPointData0 = alg_smartSurfCalc(surfParam,imgName0,imgDir,surfDir);
                surfPointData1 = alg_smartSurfCalc(surfParam,imgName1,imgDir,surfDir);
            else
                surfPointData0 = alg_smartSurfCalc(surfParam,imgName0,imgDir,surfHash);
                surfPointData1 = alg_smartSurfCalc(surfParam,imgName1,imgDir,surfHash);
            end
        else
            surfPointData0 = alg_smartSurfCalc(surfParam,imgName0,imgDir);
            surfPointData1 = alg_smartSurfCalc(surfParam,imgName1,imgDir);
        end
    else
        if useRepo
            if useSurfDir
                surfPointData0 = alg_smartSurfCalc(surfParam,imgName0,imgDs,surfDir);
                surfPointData1 = alg_smartSurfCalc(surfParam,imgName1,imgDs,surfDir);
            else
                surfPointData0 = alg_smartSurfCalc(surfParam,imgName0,imgDs,surfHash);
                surfPointData1 = alg_smartSurfCalc(surfParam,imgName1,imgDs,surfHash);
            end
        else
            surfPointData0 = alg_smartSurfCalc(surfParam,imgName0,imgDs);
            surfPointData1 = alg_smartSurfCalc(surfParam,imgName1,imgDs);
        end
        
    end
else
    surfPointData0 = alg_smartSurfCalc(surfParam,img0);
    surfPointData1 = alg_smartSurfCalc(surfParam,img1);
end

surfMatchData.surfParam = surfParam;
% matchedPoints01,inlier01,indexPairs,tform
indexPairs = matchFeatures(surfPointData0.features,...
    surfPointData1.features,'Method','Approximate');

surfMatchData.indexPairs = indexPairs;
matchedPoints0 = surfPointData0.validPoints(indexPairs(:,1));
matchedPoints1 = surfPointData1.validPoints(indexPairs(:,2));

surfMatchData.matchedPoints0 = matchedPoints0;
surfMatchData.matchedPoints1 = matchedPoints1;

try
    [tform, inlier0, inlier1] = estimateGeometricTransform(matchedPoints0, matchedPoints1, 'projective','MaxDistance',surfParam.radius,'MaxNumTrials',surfParam.maxTrials);
    numMatches = size(inlier0,1);
catch exception
    numMatches = 0;
end

surfMatchData.numMatches = numMatches;

if numMatches > 0
    surfMatchData.inlier0 = inlier0;
    surfMatchData.inlier1 = inlier1;
    surfMatchData.tform = tform;
end



if useRepo
    if rev
        surfMatchDataOld = surfMatchData;
        surfMatchData = alg_revSurfMatchData(surfMatchData);
    end
    
    if useSurfDir
        v = surfMatchData;
        save(fullfile(surfDir,surfFilename),'v');
    else
        surfHash(key) = surfMatchData;
    end
    
    if rev
        surfMatchData = surfMatchDataOld;
    end
end