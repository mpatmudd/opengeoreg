function framData = gdb_getFramingData(gdbDir,key)
%%
% Description
%   Load the framing data from the "geodatabase" for the given key.
%
% Input(s)
%   Name          Description
%   gdbDir        Path to the geodatabase
%   key           The key (a string)
%
% Output(s)
%   framData      A structure summarizing the framing (see
%                 gdb_preprocessImage)

scaleStrMain = gdb_getScaleStrMain(gdbDir);
framFile = fullfile(gdbDir,'data','framing',scaleStrMain,[key,'.mat']);
framData = load(framFile,'framData');
framData = framData.framData;
