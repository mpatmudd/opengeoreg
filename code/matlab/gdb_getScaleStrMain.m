function scaleStrMain = gdb_getScaleStrMain(gdbDir)
% Helper function to generate the string for main processing:
%
% If the main scale is 3 the result is 'scale3'
ppParam = gdb_loadPreProcParam(gdbDir);
scaleStrMain = ['scale',num2str(ppParam.mainScale)];