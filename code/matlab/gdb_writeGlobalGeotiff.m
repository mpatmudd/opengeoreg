function gdb_writeGlobalGeotiff(gdbDir,key)
% Use the global fit to write a geotiff to file for the input key.

scaleStrMain = gdb_getScaleStrMain(gdbDir);
h = gdb_getGlobalFit(gdbDir,key);
imgFile = fullfile(gdbDir,'img',scaleStrMain,[key,'.tif']);
refFile = gdb_getRefFile(gdbDir);

outImgFile = fullfile(gdbDir,'img','geotiffs','global',scaleStrMain,[key,'.tif']);
outMaskFile = fullfile(gdbDir,'img','geotiffs','global',scaleStrMain,['mask_',key,'.tif']);
img = imread(imgFile);
util_write_geotiff_using_homography(outImgFile,img,alg_homogVect2Mat(h),refFile,'outMaskFile',outMaskFile);