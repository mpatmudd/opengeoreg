function [indPrev,indNext] = gdb_getNewLink(gdbDir,W,indFit,keysAll,surfParam,scale)
% gdb_getNewLink is called when unfit images remain but no unfit images
% have known overlap with fit images. Possible matches between unfit and
% fit images are checked until overlap is detected. The results of all SURF
% matching are stored in the surfDir directory since it is an input to
% alg_smartSurfMatch.

indAll = (1:size(W,1)).';
indUnfit = setdiff(indAll,indFit);

if length(indUnfit) == 0
    indPrev = nan;
    indNext = nan;
    return;
end

done = false;

scaleStr = ['scale',num2str(scale)];
imgDir  = fullfile(gdbDir,'img',scaleStr);
surfDir = fullfile(gdbDir,'data','surf',scaleStr);


while ~done
    indPrev = datasample(indFit,1);
    indNext = datasample(indUnfit,1);
    
    if isnan(W(indPrev,indNext))
        keyPrev = keysAll{indPrev};
        keyNext = keysAll{indNext};
        imgNamePrev = [keyPrev,'.tif'];
        imgNameNext = [keyNext,'.tif'];
        
        surfMatchData = alg_smartSurfMatch(surfParam,imgNamePrev,...
            imgNameNext,imgDir,surfDir);
        
        if surfMatchData.numMatches >= surfParam.minMatch
%             W(indPrev,indNext) = surfMatchData.numMatches;
%             W(indNext,indPrev) = surfMatchData.numMatches;
            done = true;
        else
            W(indPrev,indNext) = 0;
            W(indNext,indPrev) = 0;
        end
        
    end
    
end
