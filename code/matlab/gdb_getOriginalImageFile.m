function imgFilename = gdb_getOriginalImageFile(gdbDir,key)
% Get the file that points to the original image for the given key. Either
% the original image (key.tif) or a link to it (key.txt) is stored in the
% directory */img/original

origImgDir = fullfile(gdbDir,'img','original');

fTif = fullfile(origImgDir,[key,'.tif']);
if exist(fTif,'file')
    imgFilename = fTif;
    return;
end

fTxt = fullfile(origImgDir,[key,'.txt']);
if exist(fTxt,'file')
    fid = fopen(fTxt,'r');
    imgFilename = fgetl(fid);
    fclose(fid);
    return;
else
    error('Could not find image matching this key');
end